//
// Created by sangrey on 3/14/16.
//

#include "../include/realizeddensity.h"

using arma::mat;
using arma::vec;
using arma::cube;
using arma::uword;
using arma::Mat;

namespace rd {

    std::pair<double, double> cond_moments_long_memory_prior(vec observed_vals, arma::Col<uword> lag_indices,
                                                             mat uncond_cov, double ivol_innov_var, vec ivol_coeff) {

        double cond_mean, cond_var;
        if (observed_vals.size() == lag_indices.size()) {
            cond_var = ivol_innov_var;
            cond_mean = ivol_coeff(0) + arma::dot(ivol_coeff.tail(ivol_coeff.size() -1), arma::log(observed_vals));
        } else if (observed_vals.is_empty()) {
            cond_var = uncond_cov(0, 0);
            cond_mean = ivol_coeff(0) / (1 - arma::sum(ivol_coeff.tail(ivol_coeff.size() -1)));
        } else {
            assert (lag_indices.size() >= observed_vals.size());
            double uncond_mean = ivol_coeff(0) / (1 - arma::sum(ivol_coeff.tail(ivol_coeff.size() - 1)));
            arma::uvec ovec(1, arma::fill::zeros);
            arma::uvec indices = lag_indices.head(observed_vals.size());
            cond_var = arma::as_scalar(uncond_cov(0, 0) - uncond_cov(ovec, indices) *
                                                          arma::solve(uncond_cov(indices, indices),
                                                                      uncond_cov(indices, ovec)));
            cond_mean = uncond_mean + arma::as_scalar(uncond_cov(ovec, indices) *
                                                              arma::solve(uncond_cov(indices, indices),
                                                                                     arma::log(observed_vals) - uncond_mean));
            assert (cond_var > 0);


        }
        return std::make_pair(cond_mean, cond_var);
    }

    double log_density_long_memory_prior(const vec &observed_vals, const vec &ivol_coeff,
                                         const arma::Col<uword> &lag_indices, const mat &uncond_cov, double vol,
                                         double ivol_innov_var) {

//        vec expected_vals(lag_indices.size());
//        expected_vals.head(observed_vals.size()) = arma::log(observed_vals);
//        expected_vals.tail(expected_vals.size() - observed_vals.size()).fill(ivol_coeff(0) / ( 1-
//            arma::sum(ivol_coeff.tail(ivol_coeff.size() - 1)))); // I am plugging in the unconditional expectations, for
        // unknown parameters. I really should be plugging in the conditional ones.

        double cond_mean, cond_var;
        std::tie(cond_mean, cond_var) = cond_moments_long_memory_prior(observed_vals, lag_indices, uncond_cov,
                                                                       ivol_innov_var, ivol_coeff);
//        double cond_var = cond_var_long_memory_prior(observed_vals, lag_indices, uncond_cov, ivol_innov_var);
        double return_val = scalar_normal_log_density(std::log(vol), cond_mean, std::sqrt(cond_var));
//        std::cout << "the return_val is " << return_val << std::endl;
        return return_val;
    }

    double ivol_log_prior(const vec &ivol_coeff, const vec &ivols, const arma::Col<uword> lag_indices,
                          const arma::mat &uncond_cov, double vol, const uword day, double ivol_innov_var) {

        arma::vec pretend_vols(ivols);
        pretend_vols(day) = vol;
        arma::Col<int> lags(lag_indices.size());


        double log_prior = 0;
        arma::Col<uword> arg_arr(lag_indices.size() + 1);
        arg_arr(0) = day;
        arg_arr.tail(lag_indices.size()) = day + lag_indices;


        for (auto idx : arg_arr) {
            if (idx < ivols.size()) {


                std::transform(std::begin(lag_indices), std::end(lag_indices), std::begin(lags),
                    [idx] (auto x) {return int(idx) - int(x);});

                auto last_it = std::remove_if(std::begin(lags), std::end(lags), [](double x) { return x < 0; });
                if (last_it == std::begin(lags)) {
                    log_prior += log_density_long_memory_prior(vec(), ivol_coeff, lag_indices, uncond_cov,
                                                  pretend_vols(idx), ivol_innov_var);
                }
                else {

                    vec observed_vals(std::distance(std::begin(lags), last_it));
                    std::transform(std::begin(lags), last_it, std::begin(observed_vals), ivols);

                    log_prior += log_density_long_memory_prior(observed_vals, ivol_coeff, lag_indices, uncond_cov,
                                                               pretend_vols(idx), ivol_innov_var);
                }
            }
        }

        return log_prior;
    }

    mat inv_symm_tridiag(uword n, double diagval, double alpha) {
        double normalized_alpha = alpha / diagval;
        if (std::abs(normalized_alpha - .25) < 1e-6 * std::abs(normalized_alpha) or std::abs(normalized_alpha) < std::exp(-10000 / n)) {
            return arma::inv(tridiag_generator(n, diagval, alpha));

        }
        else {

            mat returnmat(n,n);
            std::complex<double> discrimminant (1 - 4 * normalized_alpha * normalized_alpha);
            auto r_plus = 0.5 * (1.0 + std::sqrt(discrimminant));
            auto inv_diagval = std::pow(diagval, -1);
            auto inv_ratio_thing = std::pow(normalized_alpha, -1);


            if (std::abs(normalized_alpha) < .25) {
                double theta = std::log(r_plus.real() / std::abs(normalized_alpha));

            // I am computing the values using long double's because the initial can be very large in certain cases.
                // I then take tje ratio, which will reduce the size to something reasonable, but you need the extra
                // precision to calculate the initial value.
                auto sinh_power_diffs = [theta] (auto i) -> long double {return std::sinh(static_cast<long double>(((i+1) * theta)));};
                auto inv_denominator = std::pow(sinh_power_diffs(0) * sinh_power_diffs(n), -1);

                for (uword j = 0; j < n; ++j) {
                    for (uword i = j; i < n; ++i) {
                        returnmat(i, j) = static_cast<double>(-std::pow(-1.0 * sgn(normalized_alpha), i - j + 1) * sinh_power_diffs(j) *
                                        sinh_power_diffs(n - (i + 1)) * inv_denominator) * inv_ratio_thing * inv_diagval;
                    }
                }

            }
            else if (std::abs(normalized_alpha) > .25) {
                vec rescaled_power_diffs(n + 1);
                auto theta = (std::complex<double>(0, -1) * std::log(r_plus / std::abs(normalized_alpha))).real();
                for (uword i = 0; i < rescaled_power_diffs.size(); ++i) {
                    rescaled_power_diffs(i) = std::sin((i + 1) * theta);
                }


                auto inv_denominator = std::pow(rescaled_power_diffs(0) * rescaled_power_diffs(n), -1);

                for (uword j = 0; j < n; ++j) {
                    for (uword i = j; i < n; ++i) {
                        returnmat(i, j) =
                                (double) -std::pow(-1 * sgn(normalized_alpha), i - j + 1) * rescaled_power_diffs(j) *
                                rescaled_power_diffs(n - (i + 1)) * inv_ratio_thing
                                * inv_denominator * inv_diagval;
                    }
                }
            }

            return arma::symmatl(returnmat);
        }

    }

    mat tridiag_generator(uword dim, double diagval, double off_diagval) {
        mat tridiag = arma::zeros(dim, dim);
        tridiag.diag().fill(diagval);
        tridiag.diag(1).fill(off_diagval);
        tridiag.diag(-1).fill(off_diagval);

        return tridiag;
    }

    double xiu_log_likelihood(vec data, double imean, double ivol, double noise_var) {

        double diagval = ivol / data.size() + 2 * noise_var;
        double off_diagval = - noise_var;
        vec demeaned_data = data - (imean / data.size());
        double log_val = -.5 * tridiag_logdet(data.size(), diagval, off_diagval);
        log_val -= .5 * data.size() * kal::log2pi;
        log_val -= .5 * arma::as_scalar(demeaned_data.t()  * inv_symm_tridiag(data.size(), diagval, off_diagval)
                                        * demeaned_data);

        return log_val;
    }

}

double civol_log_prior(double *ivol_coeff_ptr, double *ivols_ptr, int *lag_indices_ptr, double *uncond_cov_ptr,
                       double vol, double day, double ivol_innov_var, int num_lags, int num_days) {

    const vec ivol_coeff(ivol_coeff_ptr, num_lags+1);
    const vec ivols(ivols_ptr, num_days, false, true);
    arma::Col<uword> lag_indices(num_lags);
    std::copy(lag_indices_ptr, lag_indices_ptr + num_lags+1, lag_indices.begin());
    const mat uncond_cov(uncond_cov_ptr, lag_indices.max(), lag_indices.max(), false, true);
    return rd::ivol_log_prior(ivol_coeff, ivols, lag_indices, uncond_cov, vol, day, ivol_innov_var);

}


void ctridiag_inv(int dim, double diagval, double off_diagval, double *inv_mat_ptr) {

    mat returnmat =  rd::inv_symm_tridiag((uword) dim, diagval, off_diagval);

    std::copy(std::begin(returnmat), std::end(returnmat), inv_mat_ptr);

}


double tridiag_logdet(double dim, double diagval, double off_diagval) {

    assert (diagval > 0 && "The diagonal value must be positive.  (The matrix is assumed to be psd)");

    if (off_diagval == 0) {return dim * std::log(diagval);}

    std::complex<double> discriminant = diagval * diagval - 4 * off_diagval * off_diagval;
    if (discriminant == 0.0) { return dim * std::log(.5 * diagval) + std::log(dim + 1);}

    auto r_plus = std::complex<double>(.5 * (diagval + std::sqrt(discriminant)));
    auto r_minus = std::complex<double>(.5 * (diagval - std::sqrt(discriminant)));

    auto inv_determinant = 1.0 / (r_minus * r_plus * r_plus - r_plus * r_minus * r_minus);
    arma::cx_vec coefficients = arma::cx_mat({{r_plus*r_plus, - r_plus}, {- r_minus * r_minus, r_minus}}) *
                                vec({diagval, diagval * diagval - off_diagval * off_diagval}) * inv_determinant;
    auto coeff_ratio = coefficients(0) / coefficients(1);
    auto root_ratio = r_minus / r_plus; // Note the ratios are in "opposite" directions.
    auto return_val = std::log(coefficients(1)) + dim * std::log(r_plus) +
            std::log(1.0 + coeff_ratio * std::pow(root_ratio, dim));

    return static_cast<double>(std::real(return_val));


}


double cxiu_log_likelihood(double *data_ptr, int dim, double imean, double ivol, double noise_var) {
    vec data = kal::conv_to<vec>(data_ptr, dim);

    return rd::xiu_log_likelihood(data, imean, ivol, noise_var);


}