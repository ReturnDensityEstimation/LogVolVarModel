#include <iostream>
#include <chrono>
#include <limits>
#include "../include/realizeddensity.h"



int main() {

    std::cout << "The smallest value that can be stored in a double is " << std::numeric_limits<long double>::lowest();
    std::cout << "Hello, World!" << std::endl;
    vec thing(3);
    thing << 1 << 2 << 3;

    std::cout << thing << std::endl;

//    double Delta = 1;
    double alpha = .5;
    double imean = .01;
    double ivol = 1;
    uword dim = 450;
    double diagval = 1;
    double off_diagval = 0;
    mat tridiag_mat = rd::tridiag_generator(dim, diagval, off_diagval);
    mat identity_thing = tridiag_mat * rd::inv_symm_tridiag(dim, diagval, off_diagval);
    mat data = imean / dim + (ivol / dim + 2 * alpha) * arma::randn(dim);
//    std::cout << "The identity value is"  << std::endl << identity_thing.diag() << std::endl;
//    std::cout << "the invers is " << rd::inv_symm_tridiag(dim, diagval, off_diagval) << std::endl;
//    std::cout << "Armadillo's inverse is " << arma::inv(tridiag_mat) << std::endl;

    std::chrono::time_point<std::chrono::system_clock> start1, end1;
    start1 = std::chrono::system_clock::now();
    rd::xiu_log_likelihood(data, imean, ivol, alpha);
    end1 = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end1-start1;
    std::cout << "the elapsed seconds are " << elapsed_seconds.count() << std::endl;

    std::cout << "My value for the log determinant is " << tridiag_logdet(dim, diagval, off_diagval) << std::endl;
    std::cout << "Armadillo's value is " << std::log(arma::det(tridiag_mat)) << std::endl;
    std::cout << "The log likelihood is " << rd::xiu_log_likelihood(data, imean, ivol, alpha) << std::endl;



    return 0;
}