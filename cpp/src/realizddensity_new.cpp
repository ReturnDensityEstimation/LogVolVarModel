//
// Created by sangrey on 3/14/16.
//

#include "../include/realizeddensity.h"

using arma::mat;
using arma::vec;
using arma::cube;
using arma::uword;
using arma::Mat;

namespace rd {

    double cond_var_long_memory_prior(vec observed_vals, arma::Col<uword> lag_indices, mat uncond_cov,
                                      double ivol_innov_var) {

        double cond_var;
        if (observed_vals.size() == lag_indices.size()) {
            cond_var = ivol_innov_var;
        } else if (observed_vals.size() == 0) {
            cond_var = uncond_cov(0, 0);
        } else {
            arma::uvec indices(observed_vals.size());
            arma::uvec ovec(1, arma::fill::zeros);


            assert (lag_indices.size() >= observed_vals.size());
            std::copy(lag_indices.begin(), lag_indices.begin() + indices.size(), indices.begin());
            cond_var = arma::as_scalar(uncond_cov(0, 0) - uncond_cov(ovec, indices) *
                                                          arma::solve(uncond_cov(indices, indices),
                                                                      uncond_cov(indices, ovec)));
            assert (cond_var > 0);

        }
        return cond_var;
    }

    double log_density_long_memory_prior(const vec &observed_vals, const vec &ivol_coeff,
                                         const arma::Col<uword> &lag_indices, const mat &uncond_cov, double vol,
                                         double ivol_innov_var) {

        vec expected_vals(lag_indices.size());
        for (uword i = 0; i != expected_vals.size(); ++i) {
            if (i < lag_indices.size())
                expected_vals(i) = std::log(observed_vals(i));
            else
                expected_vals(i) = ivol_coeff.at(0) / arma::sum(ivol_coeff.tail(ivol_coeff.size() - 1));
        }

        double cond_mean = ivol_coeff.at(0) + arma::dot(ivol_coeff.tail(ivol_coeff.size() - 1), expected_vals);
        double cond_var = cond_var_long_memory_prior(observed_vals, lag_indices, uncond_cov, ivol_innov_var);


        return scalar_normal_log_density(std::log(vol), cond_mean, cond_var);
    }

    double ivol_log_prior(const vec &ivol_coeff, const vec &ivols, const arma::Col<uword> lag_indices,
                          const arma::mat &uncond_cov, double vol, const uword day, double ivol_innov_var) {

        arma::vec pretend_vols(ivols);
        pretend_vols(day) = vol;

        double log_prior = 0;
        arma::Col<uword> arg_arr(lag_indices.size() + 1);
        arg_arr(0) = day;
        arg_arr.tail(lag_indices.size()) = day + lag_indices;

        for (auto idx : arg_arr) {
            std::cerr << "idx is " << idx << std::endl;
            std::vector<int> valid_lag_indices;
            for (const auto& x : lag_indices) {
                int diff = int(idx) - int(x);
                if (diff > 0) {
                    valid_lag_indices.push_back(diff);
                }
            }
            for (auto x : valid_lag_indices) {std::cout << x << std::endl;}
//            auto valid_lag_indices = lag_indices * (-1);
//            std::cerr << "valid lag indices is " << valid_lag_indices << std::endl;
//            auto last_lag = std::remove_if(std::begin(valid_lag_indices), std::end(valid_lag_indices),
//                                                 [idx](auto x) {return (int(idx) - int(x)) < 0;});


            vec observed_vals(valid_lag_indices.size());
//            if (observed_vals.size() > 0)
//            {
                std::cerr << "Day is " << day << std::endl;
//                std::for_each(std::begin(valid_lag_indices), last_lag,
//                              [idx](auto x) { std::cout << "The idx value is " << idx - x << std::endl; });
//                std::transform(std::begin(valid_lag_indices), last_lag, std::begin(observed_vals),
//                               [&](auto x) { return ivols(int(idx) - int(x)); });
//
//                double val = log_density_long_memory_prior(observed_vals, ivol_coeff, lag_indices, uncond_cov,
//                                                           pretend_vols(idx), ivol_innov_var);
//                log_prior += val;
//            }

            }
//        }
        return log_prior;
    }

}

double civol_log_prior(double *ivol_coeff_ptr, double *ivols_ptr, int *lag_indices_ptr, double *uncond_cov_ptr,
                       double vol, double day, double ivol_innov_var, int num_lags, int num_days) {

    const vec ivol_coeff(ivol_coeff_ptr, num_lags+1);
    const vec ivols(ivols_ptr, num_days, false, true);
    arma::Col<uword> lag_indices(num_lags);
    std::copy(lag_indices_ptr, lag_indices_ptr + num_lags+1, lag_indices.begin());
    const mat uncond_cov(uncond_cov_ptr, lag_indices.max(), lag_indices.max(), false, true);

    return rd::ivol_log_prior(ivol_coeff, ivols, lag_indices, uncond_cov, vol, day, ivol_innov_var);

}