//
// Created by sangrey on 3/14/16.
//

#ifndef KALMAN_REALIZEDDENSITY_H
#define KALMAN_REALIZEDDENSITY_H

#include <algorithm>
#include <cassert>
#include <complex>
#include <cmath>
#include <armadillo>
#include "/home/sangrey/Repositories/BayesianKalman/cpp/include/kalmanfilter.h"


namespace rd {

    /*
    * Computes the conditional variance of the observation.
    *  Parameters
    *  ----------
    * lag_indices : iterable of positive ints
    *   The lags of the indices.  It starts at 1 not 0.
    * observed_vals : ndarray of floats
    *   The values we observe.  It must have length less than lag_indices.
    * uncond_cov : 2d positive definite ndarray of floatsstd::pow(.5 * diagval,dim) * (dim + 1);
    *    The unconditional covariance matrix.
    * ivol_innov_var : positive float
    *    The variance of the innovation to the log-volatility equation.
    *
    * Returns
    * -------
    * double
    */
    std::pair<double, double> cond_moments_long_memory_prior(vec observed_vals, arma::Col<uword> lag_indices,
                                                             mat uncond_cov, double ivol_innov_var, vec ivol_coeff);


/*
* Computes the log prior density of a gaussian random variable with dynamics of the form y_t = \beta_0 +
* \sum_i \beta_i y_{t-i} + e_t for i in lag indices.  The unconditional covariance is the unconditional covariance of
* the model in statepsace form. See the python implementation and acompannying notes for further explanation.
*/
    double log_density_long_memory_prior(const vec &observed_vals, const vec &ivol_coeff,
                                         const arma::Col<uword> &lag_indices, const mat &uncond_cov, double vol,
                                         double ivol_innov_var);

/*
 *
 *   Calculates the value of the realized volatility density at the date given.
 *
 *   Parameters
 *   ----------std::pow(.5 * diagval,dim) * (dim + 1);
 *   vol : positive int
 *       The volatility to evaluate the model at.
 *   day : positive int
 *       The day we are considering.
 *   ivol_coeff : ndarray of floats
 *
 *   Return
 *   ------
 *   log_prior : double
 */
    double ivol_log_prior(const vec &ivol_coeff, const vec &ivols, const arma::Col<uword> lag_indices,
                          const arma::mat &uncond_cov, double vol, const uword day, double ivol_innov_var);

/*
 * Calculates the inverse of a tridiagonal symmetric toeplitz matrix analytically.
 *
 * Parameters
 * ---------
 * dim :  The dimenaionl of the matrix.
 * diagval : The value of the diagonal elements
 * alpha : The negative of the value of the off diagonal elements.
 */
    mat inv_symm_tridiag(uword dim, double diagval, double alpha);


    template<typename T>
    inline constexpr T sgn(T val) {return (val > T(0)) - (val < T(0));}

/*
 * Generates tridiagonal symmetric toeplitz matrices.
 * Parameters
 * n : dimension
 * diagval : The value of the diagonal
 * alpha : The value of the off-diagonal positive elements.
 */
    mat tridiag_generator(uword dim, double diagval, double off_diagval);


/*
 * Computes the log likelihood of the model with market microstructure noise.
 */
    double xiu_log_likelihood(vec data, double imean, double ivol, double noise_var);

}
/*
 * Provides a C interface to ivol_log_prior.
 */
extern "C"  double civol_log_prior(double *ivol_coeff_ptr, double *ivols_ptr, int *lag_indices_ptr,
                                   double *uncond_cov_ptr, double vol, double day, double ivol_innov_var, int num_lags,
                                   int num_days);


/*
 * Provides a C interface to inv_symm_tridiag
 */
extern "C" void ctridiag_inv(int dim, double diagval, double off_diagval, double *inv_mat_ptr);


/*
 * Computes the determinant of a triadiagonal topelitz matirx.
 */
extern "C" double tridiag_logdet(double dim, double diagval, double off_diagval);

/*
 * Provides a C interface to the xiu version of the log likelihood.
 */
extern "C" double cxiu_log_likelihood(double *data_ptr, int dim, double imean, double ivol, double noise_var);

#endif //KALMAN_REALIZEDDENSITY_H
