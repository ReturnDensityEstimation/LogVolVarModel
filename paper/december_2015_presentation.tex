\documentclass[fleqn,t,smaller,xcolor=dvipsnames]{beamer}
\usetheme{Warsaw}
\usecolortheme{dolphin}

\usepackage{datetime}
\usepackage{graphicx}
\usepackage{sidecap}
\usepackage{commath}
\usepackage{mathtools}
\usepackage{caption}





\setbeamertemplate{navigation symbols}{}   % Turns of the navigation symbols in the footer.
\setbeamertemplate{headline}{}      % Turns of the section/subsection names in the header.
\captionsetup{labelformat=empty}
\defbeamertemplate*{footline}{shadow theme}
{%
  \leavevmode%
  \hbox{\begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm plus1fil,rightskip=.3cm]{author in head/foot}%
    \usebeamerfont{author in head/foot}\hfill\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm,rightskip=.3cm plus1fil]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertshorttitle\hfill\insertframenumber\,/\,\inserttotalframenumber
  \end{beamercolorbox}}%
  \vskip0pt%
}


\newcommand\itembullet{
  \begingroup
  \leavevmode
  \usebeamerfont*{itemize item}%
  \usebeamercolor[fg]{itemize item}%
  \usebeamertemplate**{itemize item}%
  \setlength\labelsep   {\dimexpr\labelsep + 0.5em\relax} 
  \endgroup
}
\newcommand{\spitem}{\vspace{.3cm}\item}

\AtBeginSection[]{
{
  \setbeamertemplate{footline}{}
  \begin{frame}
  \addtocounter{framenumber}{-1}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}
}


\title[Density Prediction]{Density Prediction for Risk Measurement}
\author[Paul Sangrey]{Paul Sangrey}
\institute{University of Pennsylvania \\ \textit{sangrey@sas.upenn.edu}}
\date{\today}


\begin{document}

{
\setbeamertemplate{footline}{}
\begin{frame}[c]
\maketitle
\end{frame}
}
\addtocounter{framenumber}{-1}

\begin{frame}[c]{Preview of results}
\begin{itemize}
\item Propose a parsimonious representation of the time-varying intra-day density  of 		returns by utilizing the structure implied by no-arbitrage. 
\item Deliver density estimates and predictions thereby allowing for the calculation 		of any other quantity of interest.
\item Use Bayesian methods and high-frequency data to estimate the model and generate 		predictions from it. 
\end{itemize}
\end{frame}

\section{Why Do We Care?}

\begin{frame}[c]{GDP frequency decomposition}
Three Components:
\begin{itemize}
	\item Long-run growth
	\begin{itemize}
		\item Extremely difficult. See the joint work by M{\"u}ller and Watson.
	\end{itemize}
	\item High-frequency forecasts. 
	\begin{itemize}
		\item Tomorrow is probably the same as today. 
		\item We also do not have much information on this.
	\end{itemize}
	\item Business Cycles / Recessions
	\begin{itemize}
		\item We can predict continued growth reasonably well.
		\item We struggle with predicting changes in the direction of growth -- i.e. 						forecasting recessions. 
	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[c]{Forecasting recessions is leading case}
Timely Responses:
\begin{itemize}
	\item Bailouts, monetary policy changes, fiscal policy and so on operate with 				a lag.
	\item Business investment and other financial decisions are almost always 
		forward-looking. 
\end{itemize}
Understanding the macroeconomy:
\begin{itemize}
	\item Technological explanations for asset price movements such as the long-run 			risk and big-risks literatures require us to understand the economy's dynamic 			evolution. If we can forecast well, we can provide this.
\end{itemize}
\end{frame}

\begin{frame}[c]{What does it mean to forecast a recession?}
\begin{itemize}
	\item To forecast a recession, we have to predict a downturn in aggregate output 			during a boom. 
	\item  Economic variables are almost always highly positively autocorrelated. 
	\begin{itemize}
		\item Predicting changes is hard.
	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[c]{Probability of negative growth for U.S.}
\begin{figure}
\centering
\includegraphics[scale=.5]{figures/Pagan_Harding_Prob_Neg_Growth.jpg}
\caption{Pagan and Harding, \textit{The econometric analysis of recurrent events in economics and finance}, p. 137}
\end{figure}
\end{frame} 

\begin{frame}[c]{Probability of negative growth for U.S.}
\begin{itemize}
	\item Pagan and Harding use a probit specification and several standard 						macroeconomic variables to predict the probability of negative growth in the 			next period.
	\item Although we have a large probability of negative growth during recessions, 			this probability peaks at the end of the recession, not the beginning.
	\item If you use $1/2$ (more likely than not) as your cut-off for predicting a 				recession in the next period, this estimator does worse than the 						unconditional probability.
\end{itemize}
\end{frame}

\begin{frame}[c]{What can we do?}
\begin{itemize}
	\item Regularize more?
	\begin{itemize}
		\item Regularizing across time mechanically increases the persistence. This 				will likely make the problem worse unless we are really careful. 
	\end{itemize}
	\item Bring in more data.
\end{itemize}
\end{frame}


\begin{frame}[c]{What about financial data?}
\begin{itemize}
	\item Forward-looking.
	\item Available in real time. 
	\item It is hard to beat the market.
\end{itemize}
\end{frame}


\begin{frame}[c]{Means and volatilities}
\begin{itemize}
	\item People have done this. 
	\item They got some reasonable results.
	\item Can we do more?
\end{itemize}
\end{frame}

\begin{frame}[c]{Can we do more?}
\begin{itemize}
	\item We have thousands of observations per day.
	\item Why not estimate the entire time-varying density?
\end{itemize}
\end{frame}


\begin{frame}[c]{Density estimation}
\begin{itemize}
	\item Interesting in its own right. 
	\item Allows us to calculate anything else we want.
	\begin{itemize}
		\item Value-at-risk.
		\item Expected shortfall.
		\item Appropriately regularized realized-volatility.
	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[c]{What about realized tail measures?}
\begin{itemize}
	\item Density forecasts are mostly interesting because we can get good estimates of 		the tails.
	\item Why not just estimate the tails directly?
	\begin{itemize}
		\item Tails, by definition, occur when we do not have much data. 
		\item If we heavily regularize across time, we are mechanically making it 					harder to pick up changes. We still might want to do some of this, but we 					should be very careful.
		\item If we can regularize across the moments (i.e. estimate the density), then 			we should have less a problem with this.
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[c]{What about parametric forms for the density?}
\begin{itemize}
	\item In estimating the entire density, we use information in the center to 				estimate the tails.
	\item Per the slide above, we probably need to do this.
	\item However, we must be careful. 
	\begin{itemize}
		\item If we misspecify this relationship, the tail estimator will likely 					perform poorly since this misspecified relationship will drive the 						estimation of the tails because the center contains so much more data.
		\item If we assume a non-theoretically-founded parametric form, we will likely 				be misspecified to some degree.
	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[c]{Another viewpoint -- Dynamic Nelson-Siegel}
\begin{itemize}
	\item Dynamic Nelson-Siegel reduces the bond yield curve down to three latent 			factors.
	\item It has great forecasting performance.
	\item Can we do a similar thing for densities?
	\item What is appropriate parameterization?
\end{itemize}
\end{frame}

\section{The Density of Asset Returns}


\begin{frame}[c]{What is the structure of intra-day returns?}
\begin{itemize}
	\item No-arbitrage (i.e. no-free-lunch with vanishing risk) implies that 					he log-price process is a semimartingale.
\end{itemize}
$$p_t = p_0 + \mu t + \int_0^t \sigma(s) dW(s)$$
\end{frame}

\begin{frame}[c]{What is the It\^{o} Integral?}
$$\int_0^t \sigma(s) dW(s) \coloneqq \lim_{n \to \infty} \sum_{[t_{i-1}, t_i] \in \pi_n} \sigma(t_{i-1}) \left(W(t_{i})  - W(t_{i-1}) \right)$$
\begin{itemize}
	\item Basically a Reimann integral, but the integrator -- $dW(s)$ -- is 							random.
	\item $W(t)$ is a gaussian random variable given $t$, and so its differences 					are as well.
	\item Thus conditional on $\sigma(s)$, the increments of the	price process are 					gaussian random variables. 
\end{itemize}
\end{frame}

\begin{frame}[c]{What are log-returns?}
\begin{itemize}
	\item log-returns are simply increments of the log-price process.
	\item So conditional on the instantaneous realized volatility, the density of 				returns is an infinite gaussian mixture. 
	\item This true for any length of time.
	\item If one defines the integral by approximating with a mixture, why not estimate 			it in the same way?
\end{itemize}
\end{frame}

\begin{frame}[c]{This is what people do.}
All of the following are examples of skew-scale gaussian mixture models.
\begin{itemize}
	\item GARCH
	\item EGARCH
	\item Stochastic volatility models 
	\item Realized volatility models
	\item Skewed-t 
	\item Asymmetric Laplace
	\item Most DSGE models. 
\end{itemize}
\end{frame}

\begin{frame}[c]{Complications}
\begin{itemize}
	\item $\sigma(s)$ -- the realized volatility -- is a random function.
	\item It may be contemporaneously correlated with $\left(W(t_{i}) - W(t_{i-1})				\right)$ 
	\begin{itemize}
		\item This is the leverage effect. 
	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[c]{Takeaway}
\begin{center}
If understand the realized volatility function and the leverage effect,\\
you are done. 
\end{center}
\end{frame}

\begin{frame}[c]{Estimation}
\begin{itemize}
	\item Returns have a gaussian mixture distribution.
	\item We know how to estimate these. 
	\begin{itemize}
		\item We do have to truncate the integral somewhere -- i.e. pick the number of 					components. 
	\end{itemize}
\end{itemize}
\end{frame}

\section{The Model}


\begin{frame}[c]{Return Process}
\begin{center}
	$$\left\lbrace r_{d_t}\right\rbrace \sim  \prod_t \prod_{d_t} \sum_{k} p^k_t 			\phi\left(\mu_t + \gamma \exp(v^k_t), \exp(v^k_t)\right)$$
\end{center}
\begin{center}
\begin{tabular}{ll}
	\itembullet $V_t = M_v + \Xi_t + \Sigma_v^{1/2} U_t$  & 
	$v_t^k$ is the log-realized vol in comp $k$ in day $t$. \\
	\itembullet $\gamma \sim \mathcal{N}(0, 1.44) $ & 
	$\gamma$ governs the leverage effect. \\
	\itembullet $\mu \sim \mathcal{N}(\mu_{\mu}, \sigma^2_{\mu}) $ & 
	$\mu_t$ governs the mean return in day $t$. \\
	\itembullet $\mu_{\mu} \sim \mathcal{N}\left(0, 1.44\right)$ \\
	\itembullet $\sigma^2_{\mu} \sim \Gamma^{-1} \left(3, 2.16 \right)$ \\
\end{tabular}
\end{center}
\footnotetext[1]{I use capitalized variables for vectors and matrices, superscripts to denote components, and subscripts to denote time.}
\end{frame}


\begin{frame}[c]{Realized Volatility Process}
\begin{center}
	$$V_t = M_v + \Xi_t + \Sigma_v^{1/2} U_t $$
	$$\Xi_t =  \Phi \Xi_{t-1} +  \Sigma_{\xi}^{1/2} H_t $$
\end{center}
\begin{center}
	\begin{itemize}
		\item $M_v$ is the realized volatility's long-run mean 
		\item $M_v + \Xi_t$ is realized volatility's day $t$ mean.
		\item $U_t, H_t \sim \mathcal{N}(0, \mathbb{I}_K)$
		\item $\Phi \sim \mathcal{N}\left(.9 \mathbb{I}_K, 10 \mathbb{I}_K \right) $ 			\item $\Sigma_{\xi}\sim \mbox{Inverse-Wishart}(K + 4, .5(K + 4)\mathbb{I}_K)$ 
		\item $\Sigma_{v} \sim \mbox{Inverse-Wishart}(K + 4, .5(K + 4)\mathbb{I}_K)$ 
		\item $9.35\cdot 10^{-14} <= \exp(v_t^k) <= 7.38$
	\end{itemize}
\end{center}
\end{frame}


\begin{frame}[c]{Mixing Process}
$$K = 3 $$
$$\left( p^1_t \ldots p^K_t \right)' \sim \mbox{Dirichlet}\left(\alpha_1, \ldots, \alpha_K \right) \forall t $$
$$\left(\alpha_1, \ldots, \alpha_K \right)' \sim \left\vert \mathcal{N} \left(100 \left( 1/2^0\ldots 1/2^K\right)', 100^2 \mathbb{I}_K \right) \right\vert $$
\end{frame}



\begin{frame}[c]{The discrete-time special case}
An EGARCH model with leverage and a time-varying mean for realized volatility.
\begin{itemize}
\centering
	\item $r_{d_t} = \mu + \gamma \sigma_t + \sigma_t \eta_t$
	\item $\log(\sigma_t^2) = \mu_{v} + \xi_t + \epsilon_t$
	\item $\xi_t = \phi \xi_{t-1} + u_t$
\end{itemize}
\end{frame}

\section{The Data}


\begin{frame}[c]{Market returns}
\begin{itemize}
	\item SPY (SPDR S\&P 500 ETF Trust)
	\item Tracks the S\&P 500 in real-time with appropriate adjustements for its 					changing composition.
	\item Most traded ETF or equity. Apple is a distant second.
	\item Downloaded trades from Wharton Research Data Services' Trade and Quote 					Database.
	\item Downsampled to one-minute intervals to reduce the effect of high-frequency 				noise. 
\end{itemize}
\end{frame}

\begin{frame}[c]{Data}
\begin{itemize}
	\item Used data from 2013 for estimation and January 2014 for out-of-sample 					prediction. 
	\item 2013 had 251 trading days with an average of 448 observations per day for a 			total of 112,387 observations. 
	\item Daily log-differenced to compute the log-return and remove the 
		price discontinuity at the beginning of the day arising from overnight trading.
\end{itemize}
\end{frame}

\begin{frame}[c]{S \& P 500 Price -- 2013}
\begin{figure}
\centering
\includegraphics[scale=.5]{figures/spy_2013_price.png}
\end{figure}
\end{frame} 


\section{Estimation}


\begin{frame}[c]{Estimation}
\begin{itemize}
	\item The model is rather modular, and so a gibbs sampler should work well.
	\item Used a polya-urn representation to estimate the mixture density. 
	\begin{itemize}
		\item Added a parameter to indicate the component each datapoint belongs to, 				integrated out the probabilities, and then estimated all of the parameters 					in terms of these indicators.  
	\end{itemize}
	\item Used a Carter-Kohn multi-move gibbs sampler to draw the underlying 
		time-varying means of the realized volatilities. 
	\item Used a metropolis-hastings step to draw the particular parts of the process 			whenever a conjugate distribution was not available. 
\end{itemize}
\end{frame}

\section{Preliminary Results}


%\begin{frame}[c]{Transition matrix posterior percentiles}
%\begin{table}[t] 
%	\begin{tabular}{| c | c | c |}
%		\hline
%		\multicolumn{3}{|c|}{Transition Matrix Posterior: (5, 50, 95 percentiles)} \\
%		\hline 
%			.60 & .35 & 0.00 \\
%			(0.41, 0.78) & (0.21, 0.50) & (-0.14, 0.15) \\
%		\hline
%			0.38 & 0.66 & 0.07 \\
%			(0.23, 0.53) & (0.45, 0.78) & (-0.09, 0.24) \\
%		\hline 
%			-0.01 & 0.07 & 0.15 \\
%			(-0.20, 0.15) & (-0.12, 0.21) & (-0.05, 0.42) \\
%		\hline 
%	\end{tabular}
%\end{table}
%\footnotetext[1]{Number of simulations: 1500}
%\end{frame}

\begin{frame}[c]{Intra-day density factor structure}
\begin{figure}
\centering
\includegraphics[scale=.45]{figures/eigval_fig.png}
\caption{Real Part of Transition Matrix Eigenvalues}
\end{figure}
\end{frame}

\begin{frame}[c]{Intra-day density factor structure}
\begin{itemize}
	\item Intra-day densities have a factor structure. One parameter governs the 
		time-variation.
	\item They are very persistent, but stationary.
	\begin{itemize}
		\item Should we try a fractionally-integrated model?
	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[c]{Realized volatility estimates}
\begin{figure}
\centering
\includegraphics[scale=.45]{figures/real_vols_2013_est.png}
\end{figure}
\end{frame}

\begin{frame}[c]{Realized Volatility Estimates}
\begin{itemize}
	\item I am comparing two different estimates of the integrated volatility.
	\item My estimate is more heavily regularized and includes information from the 				sign of the return
	\item Thus the estimate should be close, but it is not obvious which one is better.
\end{itemize}
\end{frame}

\begin{frame}[c]{Return predictions}
\begin{figure}
\centering
\includegraphics[scale=.45]{figures/predicted_vals.png}
\end{figure}
\end{frame} 

\begin{frame}[c]{Prediction Quantiles}
\begin{table}
\begin{tabular}{|c|c|}
	\hline
	Predicted Interval & Realized Interval \\
	\hline 
	50 & 52.11 \\
	\hline 
	(10, 90) & (4.93, 96.66) \\
	\hline 
	(5, 95) & (1.39, 99.17) \\
	\hline
	(1, 99), & (.15, 99.86) \\
	\hline 
	(0.1, 99.9) & (0.06, 99.99) \\
	\hline
\end{tabular}
\end{table}

\end{frame}

\begin{frame}[c]{Prediction Analysis}
\begin{itemize}
	\item This comparison is for the entire month of predictions, but predictions over 			only part of the month are similar.  
	\item The interval looks reasonable but is not calibrated correctly.
	\item The prediction is based entirely on the realized volatility means.
	\begin{itemize}
		\item They are latent states extracted from a latent state.
		\item They drive the time-variation in the density.
		\item If we exploit the realized volatility's factor structure, we will have 					only one realized volatility mean parameter instead of five.
		\item This should generate significantly better predictions. 
	\end{itemize}
\end{itemize}
\end{frame}


\section{Future Work}


\begin{frame}[c]{Future Work}
\begin{itemize}
	\item Identify and correct the model misspecification.
	\item The dirichlet distribution is the parametric special case of the dirichlet 				process. So dirichlet process will produce analogous estimates of the 					density without losing much tractability or parsimony.
	\item Connect the financial risk measures to macroeconomic risk measures. 
	\begin{itemize}
		\item The realized volatility measures evolve at the daily frequency. 
		\item Thus one can take a daily measure of macroeconomic activity (such as the 					ADS Index), and include it as an observable in that part of the model and 					then extract the latent state as before. This will allow you to generate 					macroeconomic density forecasts. 
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[c]{Future Work}
\begin{itemize}
	\item The model only requires no-arbitrage and high-frequency data for 						estimation. So one should be able to apply the model to similar markets such as 		specific stocks or foreign exchange rates. 
	\item The intra-day realized volatilities, and hence densities, have a factor 				structure as do multivariate daily realized volatilities.  One can likely 				exploit this dual factor structure and the analogous multivariate no-arbitrage 			representation to produce multivariate density estimates and predictions. 
	\end{itemize}
\end{frame}

\end{document}

