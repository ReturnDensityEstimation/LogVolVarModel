\documentclass[11pt]{amsart}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts, amsthm, amssymb, latexsym, mathtools}
\usepackage{mathrsfs}
\usepackage{etex}
\usepackage{etoolbox}
\usepackage{kvoptions}
\usepackage{logreq}
\usepackage{csquotes}
\usepackage[american]{babel}
\usepackage[doublespacing]{setspace}
\usepackage[margin=1in]{geometry}
\usepackage[backend=biber, authordate]{biblatex-chicago}
%\usepackage{titling}


\addbibresource{OTC_Network_References.bib}
%\onehalfspacing


\theoremstyle{definition}
\newtheorem{Ques}{Question}
\theoremstyle{plain}
\newtheorem{Thm}{Theorem}
\pagestyle{plain}


\title{Using No-arbitrage and high-frequency data to estimate the time-varying distribution of returns}
\author{Paul Sangrey}

\begin{document}

\maketitle	

\section{Motivation}

Discuss the motivation behind a skew-scale gaussian mixture model here. It comes from the EGARCH model and the mathematical representation of no-arbitrage



\section{Thoughts on the estimation}
We first demean the returns over the entire period, and then estimate the model assuming that the mean is zero. 

\section{Notation}

I use $D_t$ to be the number of observations in each day, $T$ the total number of days and $N$ to be the total number of observations, that is $N = sum_{t=1}^T d_t$.   
I let $K$ be the number of components in the mixture. 
It is fixed for now, but I would like to allow it to vary in the future.
$\gamma$ governs the leverage in the model.  
Superscripts denote the component, and subscripts the time index.
$v^k_t$ denotes the realized log-volatility, and $\xi^k_t$ the true log-volatility for the relevant component. 
I use capital letters to denote the set of parameters. 



\section{The Model}

\begin{gather*}
K \sim Fixed \\
\left(\alpha_1, \ldots, \alpha_K \right) \sim Flat \\
\gamma \sim N(\mu_{\gamma}, \sigma^2_{\gamma}) \\
\mu \sim N(\mu_{\mu}, \sigma^2_{\mu}) \\
\mu^k_v \sim N\left( \mu_{\mu_v}, \sigma_{\mu_v} \right) \\
\sigma^2_{\xi} \sim \mbox{Inverse-Gamma}(\alpha_{\xi}, \beta_{\xi}) \\
\sigma^2_{v} \sim \mbox{Inverse-Gamma}(\alpha_{v}, \beta_v) \\
\left( \xi^1_t, \ldots, \xi^K_t \right)' \sim N\left(\Phi \left( \xi^1_{t-1}, \ldots, \xi^K_{t-1} \right)', \sigma_{\xi}^2 \cdot \mathbb{I}_K \right) \\
v^k_t  \sim N(\mu_v^k + \Xi_t^k, \sigma_{v}^2) \\
\left( p^1_t \ldots p^K_t \right) \sim \mbox{Dirichlet}\left(\alpha_1, \ldots, \alpha_K \right) \\
z_{d_t} \sim \mbox{Categorical}\left(p^1_t,\ldots p^I_t\right) \forall d_t \\
r_{d_t} \sim \prod_{t} \prod_{d_t} \prod_{k}   \exp\left( - \frac{1}{2} \delta(z_{d_t} = k) v^k_t \right) \phi\left(\frac{r_{t,d} - \mu - \gamma \exp(v^k_t)}{\exp(v^k_t/2)}\right)^{\delta(z_{d_t} = k)} \\ 
\end{gather*}


There are a few areas here that may well be misspecified. 
The most likely one is the leverage effect.  
Letting the mean of the log-return depend upon the variance is admittedly a little ad-hoc, but should be a reasonable first approximation.

\section{The Likelihood}


The model does create a hierarchical nature for the parameters that must be taken into account when estimating the model.
However, if we simply want to write down the likelihood of the data because the model is hierarchical, it simplifies quite a bit. 


\begin{align*}
P(X | Z, V, P, \gamma,\mu, A, \Xi) &= P(X | Z, V, \gamma, \mu) \\
&\propto \prod_t \prod_{d_t} \prod_k \exp \left( -\frac{1}{2}  \delta (z_{d_t} = k) v_t^k ) \right) \exp \left( - \frac{1}{2} \delta (z_{d_t}=k) \left( \frac{ \left(r_{d_t} - \mu - \gamma \exp(v^k_t)\right)^2}{\exp(v_t^k)} \right)\right) \\
&= \exp\left( \sum_t \sum_{d_t} \sum_k -\frac{1}{2} \delta(z_{d_t}=k) \left( v_t^k + \left((r_{d_t} - \mu) \exp(-v_t^k/2) - \gamma \exp(v_t^k/2)\right)^2 \right) \right) \\
\end{align*}


\section{Conditional Posterior of $v^k_t$} 

We note that the only time dependence for $V$ comes through its dependence on $\Xi$. 
Therefore, if we can condition on $\Xi$, we can estimate $v^t_k$ only on observations in that day.
In addition, if we know which component we are in, the parameters governing the other components do not matter either. 

\begin{align*}
&P(v^k_t | v^{-k}_t, V_{-t}, X, \Xi, Z, A, P \mu, \mu_v, \gamma, \sigma^2_v) \\
&= P(v^k_t | X^k_t, \xi^k_t, \gamma, \mu, \sigma^2_v ) \\
&\propto P(X^k_t | v^k_t, \gamma, \mu) P(v^k_t | \xi^k_t, \sigma^2_v, \mu_v) \\
&\propto \exp\left(-\frac{1}{2} \sum_{d_t | z_{d_t} = k}  \left( v_t^k + \left((r_{d_t} - \mu) \exp(-v_t^k/2) - \gamma \exp(v_t^k/2)\right)^2 \right) \right) \\
&\hphantom{\propto \exp\left(\right.} \cdot \exp\left(-\frac{1}{2} \frac{(v_t^k - \mu^k - \xi^k_t)^2}{\sigma^2_v} \right) \\
&= \exp\left( - \frac{1}{2} \left( \frac{(v^k_t - \mu^k - \xi^k_t)^2}{\sigma^2_v} + \sum_{d_t | z_{d_t} = k}  \left( v_t^k + \left((r_{d_t} - \mu) \exp(-v_t^k/2) - \gamma \exp(v_t^k/2)\right)^2 \right)\right)  \right) 
\end{align*}


We cannot draw directly from that function. However, we sample from the conditional likehood if we assume $\gamma = 0$, which should be close to the truth. 

\begin{align*}
&P(X_{z_{d_t} = k} | v_t^k, \mu, \gamma = 0) \\
&= \prod_{d_t | z_{d_t} = k} \exp(-\frac{1}{2} v_t^k) \exp\left( -\frac{1}{2} \frac{(r_{d_t} - \mu)^2}{\exp(v_t^k)} \right) \\ 
&= \exp(v_t^k)^{-1/2 + D_t} \exp\left( - \frac{\sum_{d_t | z_{d_t}=k} (r_{d_t} - \mu)^2}{2 \exp(v_t^k)}\right)
\end{align*}

This is simply the pdf for an inverse-gamma random variable $\exp(v_t^k)$. 
In other words,

$$\exp(v_t^k) \sim \mbox{Inverse-Gamma}\left(\frac{D_t}{2}-1, \frac{1}{2} \sum_{d_t | z_{d_t} =k} (r_{d_t} - \mu)^2 \right) $$

\section{Conditional Posterior of the probabilities}

The nice thing with the factorization above is that we do not actually have to compute the $P$ at all. 
However, if we want it for some reason, then we can use the conditional posterior below to make draws of the probabilities after we have estimated the rest of the model. 


\begin{align*}
f(P| A, Z) &\propto 
f(P, Z | A) \\
&= \prod_{t}  \left( f(p^1_t\ldots p^K_t  | \concentration^1 \ldots \concentration^K ) \prod_{d_t} f(z_{d_t} | p^1_t \ldots p^K_t ) \right)\\
&\propto \prod_{t}  \left( \prod_{i} \left( (p_t^k)^{\concentration^k-1} \right) \prod_{d_t} \prod_{i} (p_t^k)^{\delta(z_{d_t} = i)} \right)\\
&= \prod_t \prod_{k} \left( (p_t^k)^{\concentration^k-1} \right) \prod_{k} (p_t^k)^{\sum_{d_t} \delta(z_{t,d} = i)} \\
&= \prod_{t}  \prod_{i} (p_t^k)^{\concentration^k + n^k_t - 1}  \\
\end{align*}

This implies that $P | A,Z \sim \mbox{Dirichlet}(\tilde{\concentration}^1, \ldots, \tilde{\concentration}^K)$, where $\tilde{\concentration}^k = \concentration^k + n_t^k $, where $n_t^k =  \sum_{d=1}^{D_t} \mathbb{I}(z_{d_t}=k)$.
We want the $P(Z | A)$. 

\section{Conditional Posterior of Z}

To estimate the conditional posterior, we first need the conditional prior, and then we need to multiply that through by the likelihood. 
Similar algebra to that above gives the following.  

$$P(Z | A) = \prod_{t} \left( \frac{\Gamma(\sum \concentration^k)}{\Gamma(D_t + \sum \concentration^k )} \prod_{k} \frac{\Gamma(n_t^k + \concentration^k)}{ \Gamma(\concentration^k)} \right)$$.

As a result, the conditional distribution can be written as follows. 
It is worth noting that the conditional distribution of $z_{d_t}^i$ is independent of observations with different $t$ given $A$. 
$$f(z_{d_t} = k | Z^{(-d_t)}, A) \propto (n_{t}^k)^{(-d_t)} + \concentration^k $$


We now derive the posterior distribution of $Z | X, A$.  Consider the following.



\begin{align*}
&P(z_{d_t}=k | Z^{(-d_t)}, X, A, V.  \mu, \gamma)  \\
&\propto p(z_{d_t}=k| Z^{-d_t}, A) p(X | Z, V, \mu, \gamma) \\ 
&\propto \left(n_{t,d}^{(-d)} + \concentration^k \right)  \exp\left(-\frac{1}{2} \delta(z_{d_t}=k) \left( v_t^k + \left((r_{d_t} - \mu) \exp(-v_t^k/2) - \gamma \exp(v_t^k/2)\right)^2 \right) \right) \\
&\propto \left(n_{t,d}^{(-d)} + \concentration^k \right) \exp\left(-\frac{1}{2} \left( v_t^k + \left((r_{d_t} - \mu) \exp(-v_t^k/2) - \gamma \exp(v_t^k/2)\right)^2 \right) \right) \\
\end{align*} 


\section{Conditional posterior of $\gamma$}
  
The likelihood for $\gamma$ is over the Gaussian form, and so we assume a Gaussian prior in order to take advantage of the conjugacy properties. 
The derivation itself is fairly straightforward and follows standard principles. 
Because $\gamma$ only appears in the kernel of the likelihood, we can ignore the proportionality constant. 

\begin{align*}
&P(\gamma | X, M, Z, V,) \\
&\propto P(X | Z, V, \gamma, M) P(\gamma) \\
&\propto \exp\left( \sum_t \sum_{d_t} \sum_k -\frac{1}{2} \delta(z_{d_t}=k)  \exp(-v^k_t) \left((r_{d_t} - \mu_t) - \gamma \exp(v_t^k)\right)^2  \right) \exp\left(-\frac{1}{2} \frac{(\gamma - \mu_{\gamma})^2}{\sigma^2_{\gamma}} \right) \\
&\propto \exp\left(-\frac{1}{2} \left(\sum_t \sum_{d_t} \sum_{k} \delta(z_{d_t} = k)  \exp(-v_t^k) \left(\gamma^2 \exp(2v_t^k) - 2 \gamma (r_{d_t} - \mu_t) \exp(v_t^k) 
+ \frac{\gamma^2}{\sigma^2_{\gamma}} -2 \frac{\gamma \mu_{\gamma}}{\sigma^2_{\gamma}} \right)\right) \right) \\
&= \exp\left( - \frac{1}{2} \left( \gamma^2 \left(\sum_t \sum_{d_t} \sum_{k} \delta(z_{d_t} = k) \exp(v_t^k) + \frac{1}{\sigma^2_{\gamma}} \right) - 2\gamma \left( \sum_t \sum_{d_t} \sum_{k} \delta(z_{d_t} = k) (r_{d_t} - \mu_t) + \frac{\mu_{\gamma}}{\sigma^2_{\gamma}} \right) \right) \right)\\
&= \exp\left( -\frac{1}{2} \left( \gamma^2 \left(\sum_t \sum_k n_t^k \exp(v_t^k) + \frac{1}{\sigma^2_{\gamma}}\right) - 2 \gamma \left( \sum_t \sum_{d_t} r_{d_t} - \mu_t \sum_{t} D_t + \frac{\mu_{\gamma}}{\sigma^2_{\gamma}} \right) \right) \right) \\ 
\end{align*}
 
The expression above is proportion to a gaussian density. 
As a result, we have that the posterior distribution of $\gamma$ is as follows. 
I use $n_t^k$ to refer to the number of datapoints of component $k$ in day $t$. 


$$
\mathcal{N}\left( \left(\sum_t \sum_k \exp(v_t^k) n_t^k + \frac{1}{\sigma^2_{\gamma}} \right)^{-1}\left(\sum_{t} \sum_{d_t} r_{d_t} - \sum_t D_t \mu_t + \frac{\mu_{\gamma}}{\sigma^2_{\gamma}} \right),
\left(\sum_t \sum_k \exp(v_t^k) n_t^k + \frac{1}{\sigma^2_{\gamma}} \right)^{-1}\right)
$$




\section{Conditional Posterior of $\mu_t$}

We also adopt a gaussian prior for $mu_t$. 
However, it is slightly different from above. 
We adopt a hierarchical prior. 
The prior distribution of the imeans is a normal.
This is not right, but can be viewed as a form of shrinkage.
There might be a better way of doing this, but I'm not sure what it would be. 
It might be useful to do a laplace prior instead or something like that.



\begin{align*}
&P(\mu_t | X, \gamma, Z, V) \\
&\propto P(X_t | Z_t, V_t, \gamma, \mu_t) P(\mu_t | \mu_{\mu}, \sigma^2_{\mu}) \\
&\propto \exp\left( \sum_{d_t} \sum_k -\frac{1}{2} \delta(z_{d_t}=k) \left(\exp(-v_t^k) \left( \mu_t - \left(r_{d_t} - \gamma \exp(v_t^k)\right)\right)^2 \right)  \right) \exp\left( -\frac{1}{2} \frac{(\mu_t - \mu_{\mu})^2}{\sigma^2_{\mu}}\right) \\
&\propto \exp\left( -\frac{1}{2} \left( \sum_{d_t} \sum_k \delta(z_{d_t}=k) \exp(-v_t^k) \left(\mu_t^2 - 2\mu_t \left(r_{d_t} - \gamma \exp(v_t^k)\right)\right) + \left(\frac{\mu_t^2}{\sigma^2_{\mu}} - 2 \frac{\mu_t \,\mu_{\mu}}{\sigma^2_{\mu}} \right) \right)\right) \\
&\propto \exp\left( -\frac{1}{2} \left( \mu_t^2 \sum_{d_t} \sum_k \delta(z_{d_t}=k) \exp(-v_t^k) - 2 \mu_t \sum_{d_t} \sum_k \left(\delta(z_{d_t} = k)\left( \exp(-v_t^k) r_{d_t} - \gamma \right)\right) + \left(\frac{\mu_t^2}{\sigma^2_{\mu}} - 2 \frac{\mu_t \,\mu_{\mu}}{\sigma^2_{\mu}} \right) \right)\right) \\
&= \exp\left( -\frac{1}{2} \left( \sum_k n_k^t \exp(-v_t^k) + \frac{1}{\sigma^2_{\mu}} \right) \left( \mu_t - \left( \sum_{d_t} \sum_k  \delta(z_{d_t} = k) \left( \exp(-v^t_k) r_{d_t} - \gamma \right) + \frac{\mu_{\mu}}{\sigma^2_{\mu}}\right) \right) \right)\\
\end{align*}


\begin{multline*}
\mu_t | X, \gamma, Z, V, \mu_{\mu}, \sigma^2_{\mu} \sim \mathcal{N} \left( \left( \sum_{k} n_t^k \exp(-v_t^k) + \frac{1}{\sigma^2_{\mu}} \right)^{-1} \right. \\ \left.
\left(\sum_{d_t} \sum_{k} \delta(z_{d_t} = k) \exp(-v^k_t) r_{d_t} - \gamma D_t + \frac{\mu_{\mu}}{\sigma^2_{\mu}}\right),  
\left( \sum_{k}  n_k^t \exp(-v_t^k) + \frac{1}{\sigma^2_{\mu}} \right)^{-1} \right) 
\end{multline*}


\subsection{Conditional Posterior of $\mu_{\mu}$ and $\sigma^2_{\mu}$}

We adopt a conjugate prior for these parameters as well. 
As a result, we have the standard normal-inverse-gamma updating problem. 
We can update the parameters using the normal-inverse gamma conjugate process.
To draw from the posterior distribution, we can draw the $\sigma$ from its inverse-gamma marginal distribution and then $\mu_{\mu}$ from its conditional distribution. 


The prior distribution is specified as $\mu_{\mu_{\mu}}, \nu_{\mu}, \alpha_{\mu}, \beta_{\mu}$, 

We update it using the following rules. 

\begin{gather*}
\mu_{post} = \frac{\nu_{\mu} \mu_{\mu_{\mu}} + T \bar{\mu_t} }{\nu_{\mu} + T} \\
\nu_{post} = \nu_{\mu} + T \\
\alpha_{post} = \alpha_{\mu} + \frac{T}{2} \\
\beta_{post} = \beta + \frac{1}{2} \left(\sum_{t} (\mu_t - \bar{\mu_t})^2 + \frac{T  \nu_{\mu}}{\nu_{\mu} + T} (\bar{\mu_t} - \mu_{\mu_{\mu}})^2 \right)
\end{gather*}


To draw from the posterior, you can first draw an inverse-gamma from $\Gamma^{-1}(\alpha_{post}, \beta_{post})$, and then draw from the conjugate posterior of a normal distribution with known variance. 

$$\mu_{post} \sim \mathcal{N}\left(\mu_{post} , \frac{\sigma^2_{\mu}}{\nu_{post}}\right)$$ 


\section{Conditional Posterior of $A$} 

Since, $A$ only enters into the model through the use of $Z$, the conditional posterior is simply proportional to the inverse prior.  
The $\Gamma$ symbol is used below to refer to the Gamma function. 

\begin{align*}
P(A | Z) &\propto P(Z | A) \\
&= \log\left( \prod_t \left( \frac{\Gamma(\sum \concentration^k)}{\Gamma(D_t + \sum \concentration^k)}  \right) \prod_k \frac{\Gamma(n_t^k + \concentration^k)}{\Gamma(\concentration^k)} \right) \\
&= T \log\left(\Gamma\left(\sum \concentration^k\right)\right) - \sum_t \log\left(\Gamma\left(D_t + \sum \concentration^k\right)\right) + \sum_t \sum_k \left[ \log\left( \Gamma\left(n_t^k + \concentration^k\right)\right) - \log\left(\Gamma\left(\concentration^k\right)\right)\right]
\end{align*}




\end{document}


