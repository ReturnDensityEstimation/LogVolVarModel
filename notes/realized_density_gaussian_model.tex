\documentclass[11pt]{amsart}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts, amsthm, amssymb, latexsym, mathtools}
\usepackage{mathrsfs}
\usepackage{etex}
\usepackage{etoolbox}
\usepackage{kvoptions}
\usepackage{logreq}
\usepackage{csquotes}
\usepackage[american]{babel}
\usepackage[doublespacing]{setspace}
\usepackage[margin=1in]{geometry}
\usepackage[backend=biber, authordate]{biblatex-chicago}
%\usepackage{titling}


\theoremstyle{definition}
\newtheorem{Ques}{Question}
\theoremstyle{plain}
\newtheorem{Thm}{Theorem}
\pagestyle{plain}

\newcommand{\E}{\mathbb{E}}
\newcommand{\Var}{\mathbb{V}\mathrm{ar}}
\newcommand{\Cov}{\mathbb{C}\mathrm{ov}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\lequals}{\stackrel{\mathcal{L}}{=}}
\newcommand{\aequals}{\stackrel{a.s.}{=}}



\title{Using No-arbitrage and high-frequency data to estimate the time-varying distribution of returns}
\author{Paul Sangrey}

\begin{document}

\maketitle	


\section{Sampler}


The data relationships are defined as follows.  
The assumption on $Y$ comes from the paper and an assumption of conditional independence given of the rest of the realization process once we have $\mu_t$ and $\sigma_t$.  
(Later, we will consider modeled dependence).
The realized volatility likelihood is misspecified. 
However, per the Xiu paper, it will give a consistent estimator for the realized volatility in the continuous path, no-noise case.



\begin{gather*}
\left\lbrace Y_i \right\rbrace_{i=1}^n \sim \mathcal{N}\left(0, \sigma^2_t \Delta_t \mathcal{I}\right) \\
\log(\sigma_t^2) = c + \beta^{(d)} \log(\sigma^2_{t-1}) + \beta^{(w)} \log(\sigma^2_{t-7}) + \beta^{(m)} \log(\sigma^2_{t-30}) + \eta_t \\
\mu_t = \mu_{t-1} + \epsilon_t \\ 
\eta_t, \epsilon_t \stackrel{i.i.d}{\sim} \mathcal{N}(0, \sigma^2_{\sigma}) \\
\end{gather*}


The dynamics for the volatility come from using a short-lag polynomial to approximate a long-memory one, the $logs$, and normality of the innovation come from its approximate log-normality.
The timing of the lags comes from Corsi's paper. 
However, he takes averages here, while we are using the daily log-volatility in that period, and so we should have a different result. 

The specification for the mean process is rather difficult other than that it should be unpredictable since its values are not identified.
We choose a Gaussian random walk as an important leading case, but will consider several other possible specifications. 

First define $X_t := \begin{pmatrix} 1 &  \log_(\sigma^2_{t-1}) & \log_(\sigma^2_{t-7}) & \log_(\sigma^2_{t-30}) \end{pmatrix}'$, and $Y_t := \log(\sigma^2_t)$. 
Then we can estimate $\left(c, \beta^{(d)}, \beta^{(w)}, \beta^{(m)}\right)'$, jointly using a bayesian vector normal process.

To draw the $\mu_t$,  conditional on everything else, we can just do a Kalman filtering algorithm.  
We do have to worry about drawing $\sigma_\mu$, but once we know $\mu_t$ for all $t$, this is simply a method for drawing the standard deviation of normal random variates. 



The issue here is drawing the integrated volatilities.
Now, we do have one main advantage. 
If we think about the information from within the day as a likelihood, and the other days as priors we can clearly see that data within the day eventually dominates as we get enough observations per day.

The prior, however, has a rather complicated form, and it is not conjugate to the posterior.
One could represent it by a  30 dimensional state vector and evaluate the prior using the kalman filter. 
However, this would be quite wasteful.
Instead, consider the following. 

The prior is of the following form. 

$$p(y_1) \prod_{i=2}^7 p(y_i \vert y_{i-1}) \prod_{i=1}^{30} p(y_i \vert y_{i-1}, y_{i-7}) \prod_{i=8}^{30} p(y_i \vert y_{i-1}, y_{i-7}, y_{i-30}) $$

Assuming stationarity, which hold in realized volatility contexts, the first few terms are draws from a multivariate normal distribution where some of the terms are marginalized out.
Because this is a Gaussian environment, this can be done in close form.

We can put the realized volatility function in state space form in the following way.

$$\begin{pmatrix} y_{30} \\ \vdots \\ \vdots \\ \vdots \\  y_1 \\ \end{pmatrix}
= 
\begin{pmatrix} c \\ \vdots \\ \vdots \\ \vdots \\  0 \\ \end{pmatrix} 
+ 
\begin{pmatrix} \beta_1 & 0 & \cdots & 0 & \beta_7 & 0 & \cdots & 0 &  \beta_{30} \\
1 & 0 & \cdots & 0 & \cdots & 0 & \cdots &  0  & 0  \\
0 & 1 & \ddots & \ddots & \ddots & \ddots & \ddots & \ddots & \ddots \\
\vdots & 0 & \ddots & \ddots  &  \ddots & \ddots & \ddots & \ddots & \ddots \\
0 & \ddots  & \ddots & 1  &  \ddots & \ddots & \ddots & \ddots & \ddots \\
\end{pmatrix}
\begin{pmatrix} y_{29} \\ \vdots \\ \vdots \\ \vdots \\  y_0 \\ \end{pmatrix} 
+ 
\begin{pmatrix} \eta_{30} \\  0 \\ \vdots \\ \vdots \\  0 \\ \end{pmatrix}
$$


Then the unconditional distribution of $Y$, (the vector defined above), is  multivariate normal.
Let $\Phi$ and $C$ be the transition matrix and mean defined above.
Then we have the following. 

\begin{gather*}
Y \sim \mathcal{N}\left( \mu_Y, \Omega \right) \\
\mu_Y = \left( I_{30} - \Phi \right)^{-1} C  = \frac{c}{1 - \sum \Phi_{0,i}} \\
\Omega = \left( I_{30^2} - \Phi \otimes \Phi \right)^{-1} \begin{pmatrix} \sigma^2_{\eta} \\ 0  \\ \vdots \end{pmatrix} \\
\end{gather*}

It is also worth noting that we can read the conditional variances from this matrix.
In addition, since the model is Markov, we do not have to worry about conditioning on anything else.

\begin{gather*}
\Var(y_t) = \Omega_11 \\
\Var(y_t | y_{t-1} = \Omega_{11} - \Omega_{12} \Omega_{22}^{-1} \Omega_{12}' \\
\Var(y_t \vert y_{t-1}, y_{t-7}) = \Omega_{11} - \begin{pmatrix} \Omega_{1 2} \\ \Omega_{1 8} \end{pmatrix} \begin{pmatrix}\Omega_{2 2} & \Omega_{2 8}  \\ \Omega_{2 8 } & \Omega_{8 8}   \\ \end{pmatrix}^{-1} 
\begin{pmatrix} \Omega_{1 2} \\ \Omega_{1 8} \end{pmatrix}' \\
\E[y_t \vert y_{t-1}, y_{-7}] = \E[y_t] + \begin{pmatrix}\Omega_{2 2} & \Omega_{2 8}  \\ \Omega_{2 8 } & \Omega_{8 8}   \\ \end{pmatrix}^{-1} \begin{pmatrix} y_{t-1} - \E[y_t] \\ y_{t-7} - \E[y_t] \end{pmatrix}'
\end{gather*}

The conditional imeans can be read directly off of the $AR$ form of the model.
Let $z_i$ equal the variable if we know it, otherwise it equals the unconditional expectation. 
Then we have.

$$\E\left[y_t | \mbox{something} \right] = c + \beta_1 z_{t-1} + \beta_7 z_{t-7} + \beta_{t-30}$$


This reduces each of the densities defined above into their univariate representations. 


If we want to evaluate the prior for a specific time then we simply have to drop all of the times when it does not show up. 

In other words, the prior for the fourth period would be $p(y_4 | y_3) * p(y_4 | y_4) * p(y_11 | y_4) * p(y_34 | y_4)$. 


\section{Method for drawing the realized volatilities}

We use a metropolis-hastings mcmc algorithm. 
Essentially, you draw a function from a proposal distribution and then accept with the probability specified in the standard way.
If the noise variance is zero, we can actually draw directly from the likelihood which should be a very good proposal distribution because the likelihood dominates the prior asymptotically.
It's somewhat similar to selecting from a conditional prior, and the validity should follow form that argument mutadis mutandis.


In that case we have the following form for the posterior.
The likelihood for each day for the realized volatility 
$\N\left(0, \Delta_t \sigma_t^2 \mathbb{I}\right) = \frac{1}{\sqrt{2 \pi}} n^n \sigma_t^{-n} \exp\left\lbrace -\frac{n}{2 \sigma^2_t} \sum_{m=1}^n x_{t,m}^2 \right\rbrace$. 
This is proportional to the density of an inverse-gamma distributed random variable. 
It has a shape parameter $\alpha = \frac{n}{2} - 1$, and scale parameter $\beta = -\frac{n}{2} \sum_{m=1}^n x_{t,m}^2$. 

In general, however, we use the form for the likelihood given by Xiu. 
We do, however, assume that the noise term sums to $0$ in each day.
This is what is implicatively assumed by people who deal with the noise by using higher frequency data.
In addition, it is the same structure as argued by Schorfheide (And somebody) for a similar issue in consumption data. 
One cannot easily sample from this form.
However, you can still exploit the MA(1) structure.
What this means is that the covariance matrix has a symmetric, tridiagonal, toeplitz structure.
Therefore, computing the determinant is an $O(1)$ problem and the inverse (which is dense) is an $O(n^2)$.
There are some references in the computational math literature on how to do this, and that is what we do.
However, it is actually rather tricky to do this well. 
The formulas given are in the form of ratios of large powers which need to be dealt with correctly.
However, it can be done. 

As a consequence, we then use standard random walk metropolis-hastings with a proposal distribution being a 2-dimensional multivariate random normal distribution truncated at 0.
Because we are taking ratios of the proposal distribution we do not need to worry about the re-weighting because of this truncation.




\subsection{Drawing the $\beta$ coefficient for the realized volatilities}


As alluded to earlier, if we define $X_t := \begin{pmatrix} 1 &  \log_(\sigma^2_{t-1}) & \log_(\sigma^2_{t-7}) & \log_(\sigma^2_{t-30}) \end{pmatrix}'$, and $Y_t := \log(\sigma^2_t)$, we have a standard OLS regression problem. 

This is a standard Bayesian univariate regression problem.  
It gives the standard posterior. 

$$\beta \vert \sigma^2_{\epsilon}, X,Y \sim \N\left(\left(\Sigma_0^{-1} + \sigma^{-2}_{\epsilon} (X'X))^{-1}\right) \left(\Sigma_0^{-1} \beta_0 + \sigma^{-2}_{\epsilon} X'Y\right) \left(\Sigma_0^{-1} + \sigma^{-2}_{\epsilon} (X'X))^{-1}\right)\right)$$ 


The posterior distribution of $\sigma^2_{\epsilon}$ is also standard. 

$$\sigma^2_{\epsilon} \vert \beta, X,Y \sim \mbox{Inverse-Gamma}\left(\alpha_0 + \frac{n}{2}, \beta_0 + \frac{(Y - X\beta)'(Y - X\beta)}{2}\right)$$ 



\section{Draw $\mu_t$}

In this case we have the following as mentioned above. 

\begin{gather*}
\mu_t \sim \N\left(\sum x_t, \sigma_t^2\right) \\
\mu_t = \mu_0 + \mu_{t-1} + \sigma_{\eta} \eta_t \\ 
\end{gather*}


If we put this in state-space form, we have the following. 

\begin{align*}
x_t = \mu_t + \sigma_t u_t \\
\mu_t = \mu_0 + \mu_{t-1} + \sigma_{\eta} \eta_t \\ 
\end{align*}

Sine it is in a standard univariate state space model, we can estimate it using standard techniques. 


\section{$\mathbb{R}^2$ of the mean equation}

The population $\mathbb{R}^2$ is $1 - \frac{\Var(y_t | \Omega)}{\Var(y_t)}$. 

Since we have a model, we can compute the $\mathbb{R}^2$'s using the parameter estimates. 
In addition, if we take into account the relevant indices, we will have valid credible sets.

%The first relevant $\mathbb{R}^2$ is the intra-day $\mathbb{R}^2$.
%Essentially, at the end of the day, what percentage of the variation in the mean can we explain.
%Essentially, it is a particular quantification of the lack of identification.
%(In practice, since we know that the drift is not identified from the price process, we should not assume that practitioners necessarily know it. 
They very %well might not.)

%$\Var(y_t \vert \Omega_t)$ is just the innovation variance.  
%(This doesn't work.  
%I am assuming that you know yeterday's $\mu_t$. 
%That doesn't work, if I am assuming that you don't know todays.....


If we look at the one-step ahead $\mathbb{R}^2$, we can simply calculate the denonimator by computing the unconditional variance across samples. 
To get the numerator, we can do the following. 

\begin{align*}
\Var(y_t \ \Omega_{t-1} ) &= 
\E\left[\left(y_t - \E\left[y_t \middle\vert \Omega_{t-1} \right]\right)^2\right] \\ 
&= \E\left[\left(y_t - \E\left[ y_t \middle\vert \Omega_{t-1} \log(\sigma^2_t \right]   + \E\left[ y_t \middle\vert \Omega_{t-1} \log(\sigma^2_t \right] - \E\left[y_t \middle\vert \Omega_{t-1} \right]\right)^2\right] \\
&= \E\left[ \left(\sigma_{\mu} \eta_t + \gamma_{\sigma} \sigma_{\sigma} u_t \right)^2\right] \\
&= \sigma^2_{\eta} + \gamma_{\sigma}^2 \sigma^2_{\sigma} 
\end{align*}




\end{document}


