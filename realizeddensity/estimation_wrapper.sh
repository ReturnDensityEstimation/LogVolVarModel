#!/bin/bash 
unset modules
# Exports evnrionment vairables
#$ -V

# Execute from current directory 
#$ -cwd

#Specifies that MPI will be used with 24 workers
#$ -pe threaded 12

###############################
###############################
/usr/bin/time python ./estimation.py
