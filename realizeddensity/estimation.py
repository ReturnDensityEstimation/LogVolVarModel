import numpy as np
from scipy import stats
import pandas as pd
import os
from realizeddensity import gaussian_process_model as gpm
from bayesiankalman import mcmc
from tqdm import tqdm
from multiprocessing import Pool


def imean_prior_flat(imean):
    # mean_val = 1
    # ar_val = stats.uniform.pdf(imean[1], -1, 2)

    # return mean_val * ar_val
    # return 1
    priorval = stats.multivariate_normal.pdf(x=np.atleast_2d(imean), mean=np.zeros(4), cov=.0625 * np.eye(4))
    priorval *= float(imean[0] >= 0)
    #TODO Figure out why variables with phi1 + phi2 < -1 aren't being rejected.
    if imean[1]**2 + 4 * imean[2] <= 0:
        priorval *= float(imean[2] > - 1)
    else:
        priorval *= float((imean[2] > -1) * (imean[2] - imean[1] < 1) * (imean[2] * imean[1] < 1))
    # priorval *= float((np.abs(imean[1] + imean[2]) <= 1))
    return priorval


def imean_prior_rounded(imean):
    mean_val = stats.norm.pdf(imean[0], loc=0, scale=.01)
    var_val = stats.uniform.pdf(imean[1], -1, 2) * stats.norm.pdf(imean[0], loc=0, scale=.1)

    return mean_val * var_val


if __name__ == "__main__":

    multiple_days = False
    simulate = True
    result_file = '../results/cond_gp_model.tmp2.hdf'
    nsims = 1000
    horizon = 0
    pre_forecast_period_length = 1000

    if simulate:
        num_obs_per_day = 450
        starting_date = '2005-01-01'
        num_days = 500
        assert (86400.0 / num_obs_per_day).is_integer(), 'There needs to tbe the same number of integers every day.'

        date_index = pd.date_range(starting_date, periods=num_obs_per_day * num_days,
                                   freq=(str(int(86400 / num_obs_per_day)) + 's'))

        simulated_model = gpm.simulate_gp_model(ivol_coeff=np.array([-.42, .43, .21, .11, .05, .07, .04, .03, .02]),
                                                lag_indices=np.array([1, 2, 3, 4, 7, 14, 21, 28]),
                                                imean_innov_var=5.0e-7,
                                                ivol_innov_var=.30,
                                                num_periods=num_days,
                                                num_obs_per_day=num_obs_per_day,
                                                imeans_coeff=[1.5e-4, -.80, .04, -4.3e-5],
                                                leverage=True,
                                                intra_day_log_vol_var=0
                                                )

        entire_data = pd.Series(simulated_model.data.ravel(), date_index.to_pydatetime().astype('datetime64[s]'))
    else:
        dates1 = '2005-01'
        dates2 = '2012-12'
        entire_data = pd.read_hdf('../data/spy_rtn_for_daily_rtn_2001_2012_clean.hdf', 'table')[dates1:dates2]
        num_obs_per_day = np.argmax(np.bincount(entire_data.groupby(pd.TimeGrouper('D')).count()))

    if multiple_days:
        days = entire_data.groupby(pd.TimeGrouper('D')).sum().dropna().index
    else:
        days = [pd.Timestamp('2007-01-03')]

    begin_day = entire_data.index[0].replace(hour=0, minute=0, second=0, microsecond=0)

    prior = { 'ivols': {'ivol_coeff': {'mean': np.asarray([-1, .5, 0, 0, 0, 0, 0, 0, 0]),
                                      'cov': np.diag([5.0, 1, 1, 1, 1, 1, 1, 1, 1])},
                       'innov_var': {'shape': 3, 'scale': .3}
                       },
             'imeans': { 'innov_var': {'shape': 3, 'scale': 2.1e-4},
                          'imean_coeff': imean_prior_flat
                        }
             }

    grouped_data = entire_data.groupby(pd.TimeGrouper('D'))

    day_index = np.array([np.datetime64(index) for index, group in grouped_data
                          if group.size == num_obs_per_day]).astype('datetime64[D]')
    data_df = pd.DataFrame([np.ravel(group.values) for _, group in grouped_data if group.size == num_obs_per_day],
                           index=day_index)

    timestamps = np.asarray([[np.datetime64(time) for time in group.index] for date, group in
                             grouped_data if group.size == num_obs_per_day])
    ranges = (np.asarray([np.timedelta64(np.amax(day) - np.amin(day), 's') for day in timestamps]) /
              np.timedelta64(1,'s'))
    ranges /= np.mean(ranges)
    ranges_df = pd.DataFrame(ranges, index=day_index)


    def estimate(day, only_forecast=False):
        day_np = day.to_pydatetime().strftime('%B_%d_%Y')
        result_vals = {day_np: {'dates': data_df.loc[begin_day:day].index.values.astype(
            'datetime64[D]').astype(np.float64)}}
        gp_model = gpm.GPModel(data=data_df.loc[:day],
                               ivol_coeff=np.array([-.5, 0, 0, 0, 0, 0, 0, 0, 0]),
                               lag_indices=np.array([1, 2, 3, 4, 7, 14, 21, 28]),
                               imean_innov_var=5e-7,
                               ivol_innov_var=.1,
                               imean_coeff=[0, 0, 0, 0],
                               ranges=ranges_df.loc[:day],
                               leverage=True,
                               noise_vars=False
                               )

        parameter_estimates = mcmc.estimate_model(gp_model, nsims, prior)
        if not only_forecast:
            result_vals[day_np].update(parameter_estimates)

        if horizon > 0:
            forecasted_data, forecasted_ivols, forecasted_imeans = gpm.forecast_draws(parameter_estimates, gp_model,
                                                                                      horizon)
            forecast_dict = {'forecasted_data': forecasted_data, 'forecasted_imeans':
                             np.squeeze(forecasted_imeans), 'forecasted_ivols': forecasted_ivols}
            result_vals[day_np].update(forecast_dict)

        return result_vals


    try:
        os.remove(result_file)
    except FileNotFoundError:
        pass

    print('The number of days is ' + str(len(days)))
    mcmc.save_hdf({'values': entire_data.values, 'index':
                   entire_data.index.to_pydatetime().astype('datetime64[s]').astype(np.float64)}, result_file,
                  group='data')
    mcmc.save_hdf(estimate(days[-1], only_forecast=False), result_file, group='results')

    if horizon > 0:
        with Pool(16) as p:
            for results in tqdm(p.imap_unordered(estimate, days[pre_forecast_period_length:-1])):
                mcmc.save_hdf(results, result_file, group='results')

    if simulate:
        # noinspection PyUnboundLocalVariable,PyUnboundLocalVariable
        mcmc.save_hdf({'values': simulated_model.ivols, 'index':
                       entire_data.resample('D').mean().index.to_pydatetime().astype('datetime64[s]').astype(np.float64)},
                      result_file, group='ivols')
        mcmc.save_hdf({'values': simulated_model.imeans, 'index':
                       entire_data.resample('D').mean().index.to_pydatetime().astype('datetime64[s]').astype(np.float64)},
                      result_file, group='imeans')

    print('Everything seemed to work!')
