""" This module contains code to estimate the gaussian process model described in the accompanying notes. """

import functools
import os
import numpy as np
from cffi import FFI
from scipy import stats, linalg as scilin
from bayesiankalman import mcmc
from bayesiankalman.kalmanfilter import kalman_smoother

ffi = FFI()
ffi.cdef("""double civol_log_prior(double *ivol_coeff_ptr, double *ivols_ptr, int *lag_indices_ptr, double *uncond_cov_ptr,
                       double vol, double day, double ivol_innov_var, int num_lags, int num_days);""")
ffi.cdef("""void ctridiag_inv(int dim, double diagval, double off_diagval, double *inv_mat_ptr);""""")
ffi.cdef("""double tridiag_logdet(double dim, double diagval, double off_diagval);""""")
ffi.cdef("""double cxiu_log_likelihood(double *data_ptr, int dim, double imean, double ivol, double noise_var);""")
path = os.path.dirname(os.path.dirname(__file__))
realizeddensity_so = ffi.dlopen(path + '/lib/librealizeddensity.so')


# noinspection PyTypeChecker
class GPModel:
    """ Holds the state of the sampler and provides methods to draw from the posterior. """

    def __init__(self, data, ivol_coeff=None, lag_indices=None, imean_innov_var=1, ivol_innov_var=None, imeans=None,
                 ivols=None, imean_coeff=None, ranges=None, leverage=False, sampler_cov=None, noise_vars=False):

        self._data = np.atleast_2d(data)
        self._ivol_object = IntegratedVolatility(ivols=ivols, noise_vars=noise_vars, ivol_coeff=ivol_coeff, data=data,
                                                 lag_indices=lag_indices, innov_var=ivol_innov_var, ranges=ranges,
                                                 sampler_cov=sampler_cov)
        self._imean_object = IntegratedMean(imeans=imeans, mean_coeff=imean_coeff, data=data,
                                            innov_var=imean_innov_var, leverage=leverage)

    def __call__(self, prior):
        """
        Returns a draw from the posterior and updates the state.

        Returns
        -------
        draw: dictionary of the parameters and whether they were accepted.
        """
        # Tracer()()
        draw = {}
        if 'ivols' in prior:
            draw.update(self._ivol_object(ivol_lg_prior=self.ivol_log_prior, data=self.data, prior=prior['ivols'],
                                          imeans=self.imeans))

        if 'imeans' in prior:
            draw.update(self._imean_object(data=np.sum(self.data, axis=1),
                                           ivols=self.ivols,
                                           prior=prior['imeans'],
                                           ivol_coeff=self.ivol_coeff))

        return draw

    def ivol_log_prior_mean_part(self, vol, day):

        num_mean_lags = self.imean_coeff.size - 2
        ivol_coeff = self.imean_coeff[-1]

        log_pdf = stats.norm.logpdf(np.log(vol), self.imean_coeff[:-1].dot([1,self.imeans[day-num_mean_lags:day]]) / ivol_coeff,
                                    ivol_coeff * np.sqrt(self.imean_innov_var))

        return log_pdf

    def ivol_log_prior(self, vol, day, ivol_coeff, include_mean_part=False):
        """
        Calculates the value of the realized volatility density at the date given.

        Parameters
        ----------
        vol : positive int
            The volatility to evaluate the model at.
        day : positive int
            The day we are considering.
        ivol_coeff : ndarray of floats
        include_mean_part : bool, optional
            Whether to include the effect of the mean in computing the mean.
        Returns
        -------
        ivol_log_prior : double
        """
        prior_val = ivol_log_prior(ivol_coeff, self.ivols, self.lag_indices, self.uncond_cov, vol, day,
                              self.ivol_innov_var)

        if include_mean_part:
            prior_val += self.ivol_log_prior_mean_part(vol, day)

        return prior_val

    def forecast(self, horizon):
        """
        Generates forecasts from the model.

        It rolls the ivol and imean processes forward and then generates data from the statespace model.

        Parameters
        ----------
        horizon : positive int

        Returns
        -------
        future_data : ndarray
        future_ivols: ndarray
        future_imeans : ndarray
        """
        model = simulate_gp_model(ivol_coeff=self.ivol_coeff, lag_indices=self.lag_indices,
                                  imean_innov_var=self.data_innov_var, ivol_innov_var=self.ivol_innov_var,
                                  num_periods=horizon, num_obs_per_day=self.num_obs_per_day,
                                  imeans_coeff=self.imean_coeff, initial_ivol=self.ivols[-np.amax(self.lag_indices):],
                                  initial_imean=self.imeans[-1], leverage=True)

        future_data = model.data
        future_ivols = model.ivols
        future_imeans = model.imeans

        return future_data, future_ivols, future_imeans

    @property
    def uncond_cov(self):
        ivol_coeff = tuple(np.ravel(self.ivol_coeff))
        return compute_uncond_cov(ivol_coeff, tuple(self.lag_indices),
                                  np.asscalar(np.asarray(self.ivol_innov_var)))

    @property
    def num_periods(self):
        return self.data.shape[0]

    @property
    def num_obs_per_day(self):
        return self.data.shape[1]

    @property
    def ivols(self):
        return self._ivol_object.ivols

    @ivols.setter
    def ivols(self, val):
        self._ivol_object.ivols = val

    @property
    def imeans(self):
        return self._imean_object.imeans

    @imeans.setter
    def imeans(self, val):
        self._imean_object.imeans = val

    @property
    def data_innov_var(self):
        return self._imean_object.innov_var

    @data_innov_var.setter
    def data_innov_var(self, val):
        self._imean_object.innov_var = val

    @property
    def lag_indices(self):
        return self._ivol_object.lag_indices

    @lag_indices.setter
    def lag_indices(self, val):
        self._ivol_object._lag_indices = val

    @property
    def data(self):
        return self._data

    @property
    def ivol_coeff(self):
        return self._ivol_object.ivol_coeff

    @property
    def imean_coeff(self):
        return self._imean_object.imean_coeff

    @property
    def imean_innov_var(self):
        return self._imean_object.innov_var

    @property
    def drift_innov_var(self):
        return self._imean_object.innov_var

    @property
    def ivol_innov_var(self):
        return self._ivol_object.innov_var

    @ivol_innov_var.setter
    def ivol_innov_var(self, val):
        self._ivol_object.innov_var = val


# noinspection PyTypeChecker
class IntegratedMean:
    """ This class holds the mean and contains methods to update its state  """

    # noinspection PyUnresolvedReferences
    def __init__(self, imeans=None, mean_coeff=0, data=None, innov_var=1, num_periods=None, leverage=None):
        """
        Setups the class with given parameters or reasonable defaults.

        Parameters
        ----------
        imeans : array-like or double
            The time-varying mean of the process.

        data : 1d or 2d array of floats
            The data we are considering. It if is two dimensional the first dimension is assumed to be the number of
            days, and the second the observations per day. Otherwise, we have one observation per day.
        num_periods : positive int
        leverage: double, optional
        """
        if imeans is not None:
            self.imeans = np.asarray(imeans)
        elif data is not None:
            self.imeans = np.asarray(np.sum(data, axis=1)) / 10 if data.ndim == 2 else np.asarray(data) / 10
        elif num_periods is not None:
            self.imeans = np.zeros(num_periods)
        else:
            raise ValueError('Incorrect Arguments. Either imeans, data or num_periods must be given.')

        self.innov_var = np.asarray(innov_var)
        self.imean_coeff = np.asarray(mean_coeff)
        self.leverage = leverage
        self.counter = 0

    def __call__(self, data, prior, ivols=None, ivol_coeff=0):
        """
        Returns a dictionary of draws from the conditional posterior of its parameters, and updates its state.

        Parameters
        ----------
        ivol_coeff
        data : 2d array-like
        ivols : 1d array-like, optional
            The integrated volatility in each day.
        ivol_coeff : 1d array-like, optional

        Returns
         -------
        draw : dict
            The parameters that are drawn
        """
        self.imeans = self.draw_means(data, ivols, ivol_coeff)

        if 'imean_coeff' in prior:
            self.imean_coeff = (self.draw_imean_coeff(prior['imean_coeff'], ivols, ivol_coeff) if self.leverage
                                else self.draw_imean_coeff(prior['imean_coeff']))
        self.counter += 1
        if 'innov_var' in prior: # and self.counter > 100:
            self.innov_var = self.draw_innov_var(prior['innov_var'], ivols, ivol_coeff=ivol_coeff)

        draw = {'imean_innov_var': self.innov_var, 'imean_coeff': self.imean_coeff.copy(),
                'imeans': self.imeans.copy()}

        return draw

    def draw_means(self, data, ivols, ivol_coeff=0):
        """
        Uses a kalman smoothing algorithm to draw the time-varying imeans.

        Parameters
        ----------
        ivol_coeff
        data : 2d array-like
        ivols : 1d array-like
            The integrated volatility in each day.

        Returns
        -------
        draw = ndarray
            A draw from the conditional posterior of the imeans.

        """
        if self.leverage:
            ivol_coeff = np.ravel(ivol_coeff)
            log_vol_uncond_mean = ivol_coeff[0] / (1 - np.sum(ivol_coeff[1:]))
            drift = self.imean_coeff[0] + self.imean_coeff[-1] * (np.log(ivols) - log_vol_uncond_mean)
            state_dim = self.imean_coeff.size - 2
            state_mean = np.hstack([drift.reshape(self.num_periods,1), np.zeros((self.num_periods, state_dim-1))])

        else:
            drift = self.imean_coeff[0]
            state_dim = self.imean_coeff.size - 1
            state_mean = np.hstack([drift, np.zeros(state_dim-1)])


        data_loadings = np.hstack([1, np.zeros(state_dim - 1)])
        state_trans = mcmc.factor_model_state_trans(self.imean_coeff[1:state_dim+1])
        state_innov_var = np.zeros((state_dim, state_dim))
        state_innov_var[0,0] = self.innov_var

        draw, _, _ = kalman_smoother(data=data,
                                     data_loadings=data_loadings / 10,
                                     data_mean=0,
                                     data_innov_var=np.mean(ivols.reshape(self.num_periods,1)),
                                     state_trans=state_trans,
                                     state_mean=state_mean * 10,
                                     state_innov_var=state_innov_var * 100,
                                     stationary=False,
                                     time_varying=True
                                     )
        imeans =  np.squeeze(draw[:,0] / 10)
        return imeans

    def draw_innov_var(self, prior, ivols, ivol_coeff=0):
        """
        Uses conjugacy to return a draw from the conditional posterior of the data innovation variance.

        Parameters
        ---------
        ivol_coeff
        prior : dict
            Contains the prior parameters.
        ivols : array-like
        ivol_coeff: array-like, optional
        Returns
        -------
        draw, positive double
        """

        if self.leverage:
            num_lags = self.imean_coeff.size -2
            ivol_coeff = np.ravel(ivol_coeff)
            log_vol_uncond_cov = ivol_coeff[0] / (1 - np.sum(ivol_coeff[1:]))
            regressor = mcmc.create_lag_matrix(data=self.imeans,
                                               lags=num_lags,
                                               external_regressors=np.log(ivols) - log_vol_uncond_cov,
                                               prepend_ones=True)
        else:
            num_lags = self.imean_coeff.size -1
            regressor = mcmc.create_lag_matrix(self.imeans, num_lags, prepend_ones=True)

        draw = mcmc.draw_innovation_var(regressor, self.imeans[num_lags:], self.imean_coeff, prior['scale'],
                                        prior['shape'])

        return draw

    def draw_imean_coeff(self, prior, ivols=None, ivol_coeff=None):
        """
        Draws the imean_coeff parameter from its conditional posterior.

        It estimates both the autoregressive and drift parameters.

        We accept three different types of priors. If prior['var'] is a scalar it is assumed to be the variance of the
        imean's drift, and the autoregressive coefficient is assumed to be fixed. Else if prior is callable, we
        assume that it returns the prior of the variance, and a metropolis hastings step is used. Otherwise, we
        assume that it is a positive-definite covariance matrix for the prior coefficients.

        Parameters
        ----------
        ivol_coeff
        ivols
        prior : dict
        ivols : array-like, optional
        ivol_coeff : array-like, optional
            If ivols is given, ivol_coeff must be given as well.

        Returns
        -------
        draw : 1d ndarray
        """

        if self.leverage and callable(prior):
            ivol_coeff = np.ravel(ivol_coeff)
            num_lags = self.imean_coeff.size-2
            uncond_mean = ivol_coeff[0] / (1 - np.sum(ivol_coeff[1:]))
            regressor = mcmc.create_lag_matrix(data=self.imeans,
                                               lags=num_lags,
                                               external_regressors=np.log(ivols) - uncond_mean,
                                               prepend_ones=True)
        else:
            num_lags = self.imean_coeff.size-1
            regressor = mcmc.create_lag_matrix(self.imeans,num_lags, prepend_ones=True)

        regressand = self.imeans[num_lags:]
        draw = self.coeff_draw(prior, regressand, regressor)

        return draw

    def coeff_draw(self, prior, regressand, regressor):
        """
        Computes the coefficient by regressing regrassand and regressor and then re-weighting by the prior.

        Parameters
        ----------
        prior : callable
        regressand : ndarray
        regressor : ndarray

        Returns
        -------
        draw : ndarray
        """
        draw = mcmc.draw_matrix_coefficient(prior_mean=np.zeros_like(self.imean_coeff),
                                            prior_var=np.zeros((self.imean_coeff.size, self.imean_coeff.size)),
                                            regressand=regressand,
                                            regressor=regressor,
                                            innovation_var=self.innov_var)
        metrop_alpha = np.exp(np.log(prior(draw)) - np.log(prior(self.imean_coeff)))
        if not metrop_alpha > np.random.uniform():
            draw = self.imean_coeff

        return np.squeeze(draw)

    @property
    def num_periods(self):
        return self.imeans.shape[0]


# noinspection PyTypeChecker
class IntegratedVolatility:
    """ This class contains methods to hold the realized volatilities and update its state.   """

    # noinspection PyTypeChecker
    def __init__(self, ivols=None, noise_vars=False, ivol_coeff=None, data=None, num_periods=None, lag_indices=None,
                 innov_var=None, ranges=None, sampler_cov=None):
        if ivols is not None:
            if np.isscalar(ivols) and num_periods is not None:
                self._ivols = np.full(num_periods, fill_value=ivols, dtype=np.double)
            self._ivols = np.asarray(ivols, dtype=np.double).ravel()
        elif data is not None:
            self._ivols = np.ascontiguousarray(np.asarray(data).shape[1] *  np.var(data, axis=1), np.float64)
        else:
            raise ValueError('Either data or ivols must be specified.')
        self.ivol_coeff = (np.asarray(ivol_coeff).ravel()
                           if ivol_coeff is not None else np.array([.1 * np.mean(data), .9, 0, 0]))
        self.lag_indices = np.asarray(lag_indices, dtype=np.int).ravel() if lag_indices is not None \
            else np.array([1, 7, 30])
        assert self.ivol_coeff.size == self.lag_indices.size + 1, 'The volatility coefficient and lag indices have ' \
                                                                  'incompatible sizes. You may have forgotten to' \
                                                                  ' include an intercept.'
        if noise_vars is True:
            self.noise_vars = .1 * self.ivols
        elif noise_vars is not False and np.isscalar(noise_vars):
            self.noise_vars = np.full(self.num_periods, noise_vars, dtype=np.float64)
        elif noise_vars is not None and noise_vars is not False:
            self.noise_vars = np.ravel(noise_vars)
            if self.noise_vars.size != self.num_periods:
                raise ValueError('The noise vars that you gave are not the correct size.')

        self.sampler_cov = sampler_cov if sampler_cov is not None else .2 * np.std(self.ivols) * np.eye(2)
        self.sampler_scale = .5
        self.innov_var = np.asarray(innov_var) if innov_var is not None else np.mean(self.ivols ** 2) / 2
        self.ranges = np.ravel(ranges) if ranges is not None else np.ones(self.num_periods)
        self.acceptance_rate = np.array(0)
        self.counter = 0

    def __call__(self, data, ivol_lg_prior=None, prior=None, imeans=0):
        """
        Updates the state and returns a draw from the posterior.

        Parameters
        ----------
        ivol_lg_prior : function
            Takes the volatility and the day and returns the log prior density. It must be a function of the volatility
            and the day.
        data : 2d ndarray of floats
            The data organized into num_periods rows and num_obs columns.

        Returns
        -------
            dictionary of draws.

        """
        draw = {}
        self.counter += 1
        if ivol_log_prior is not None:
            # if False:
            ivols_draw = self.draw_ivols(data=data, imeans=imeans,
                                         lg_prior=functools.partial(ivol_lg_prior, ivol_coeff=self.ivol_coeff))
            self.acceptance_rate = ivols_draw['ivols_accepted']
            self.ivols = ivols_draw['ivols']
            if 'noise_vars' in ivols_draw:
                self.noise_vars = ivols_draw['noise_vars']
                draw.update({'noise_vars': self.noise_vars.copy()})
                if 5 < self.counter < 50:
                    self.sampler_cov = np.cov(self.ivols, self.noise_vars)
                    if self.acceptance_rate < .2:
                        self.sampler_scale *= .8
                    elif self.acceptance_rate > .5:
                        self.sampler_scale *= 1.2

        if prior is not None and 'ivol_coeff' in prior:
            ivol_coeff_draw = self.draw_ivol_coeff(prior['ivol_coeff'])
            self.ivol_coeff = ivol_coeff_draw['ivol_coeff']

        if prior is not None and 'innov_var' in prior:
            innov_var_draw = self.draw_innov_var(prior['innov_var'])
            self.innov_var = innov_var_draw['ivol_innov_var']

        draw.update({'ivols_accepted': self.acceptance_rate.copy(), 'ivols': self.ivols.copy(),
                     'ivol_coeff': self.ivol_coeff.copy(), 'ivol_innov_var': self.innov_var.copy()})

        return draw

    def draw_ivol_coeff(self, prior):
        """
        Returns a draw from the posterior of volatility's imean_coeff coefficient.

        Parameters
        ----------
        prior : dict of floats
            The prior parameters
        Returns
        -------
        draw : dict of floats
        """
        #TODO Split this into a deviations from the mean and other thing components, and think about switching to an
        #explicitely long-memoery model.
        try:
            regressor = mcmc.create_lag_matrix(np.log(self.ivols), self.lag_indices, prepend_ones=True)
        except ValueError as e:
            raise ValueError('The maximum lag value cannot be longer than the data are.').with_traceback(
                e.__traceback__)

        regressand = np.log(self.ivols[-regressor.shape[0]:])
        post_coeff = np.squeeze(mcmc.draw_matrix_coefficient(regressand=regressand, regressor=regressor,
                                                             prior_mean=prior['mean'], prior_var=prior['cov'],
                                                             innovation_var=self.innov_var))
        draw = {'ivol_coeff': post_coeff}
        return draw

    # noinspection PyTypeChecker,PyTypeChecker
    def draw_ivols(self, data, lg_prior, imeans=0):
        """
        Draws the integrated volatility.

        It uses the ivol_log_prior argument as the prior function and the likelihood as described in the notes.
        Parameters
        ----------

        data : 2d array of floats
        lg_prior : function
            This the ivol_log_prior.  It must already have all of its arguments except for the volatilities bound to it.
        imeans : float or array-like, optional
            Gives the value of the mean in each day.
        Returns
        -------
        results : dict
            Contains a draw of the posterior parameters.
        """

        draw = np.ravel(self.ivols.copy())
        results = {}
        acceptance_arr = np.zeros(self.ivols.size, dtype=np.int)
        num_obs_per_day = data.shape[1]

        imeans = (np.full(self.num_periods, imeans, dtype=np.float64) if np.isscalar(imeans)
                  else imeans.astype(np.float64))
        if hasattr(self, 'noise_vars'):
            noise_vars = np.ravel(self.noise_vars.copy())
            for day, row in enumerate(data):
                old_draw = np.array([self.ivols[day], self.noise_vars[day]])
                proposal_dist = stats.multivariate_normal(mean=old_draw, cov=self.sampler_cov * self.sampler_cov)
                proposal = np.abs(proposal_dist.rvs())
                new_weight = (proposal_dist.logpdf(proposal) + lg_prior(proposal[0], day) +
                              tridiag_log_like(row, imeans[day], proposal[0], proposal[1]))
                old_weight = (proposal_dist.logpdf(old_draw) + lg_prior(old_draw[0], day) +
                              tridiag_log_like(row, imeans[day], old_draw[0], old_draw[1]))
                if np.exp(new_weight - old_weight) > np.random.uniform():
                    # We accept the draw.
                    draw[day] = proposal[0]
                    acceptance_arr[day] = True
                    noise_vars[day] = proposal[1]
            results.update({'noise_vars': noise_vars})
        else:
            shape = num_obs_per_day / 2 - 1
            for day, row in enumerate(data):

                mean = imeans[day] / num_obs_per_day
                post_scale = (num_obs_per_day / 2) * np.sum((row - mean)**2)
                proposal = stats.invgamma.rvs(a=shape, scale=post_scale)
                new_weight = lg_prior(proposal, day)
                old_weight = lg_prior(self.ivols[day], day)

                if np.exp(new_weight - old_weight) > np.random.uniform():
                    # We accept the draw.
                    draw[day] = proposal
                    acceptance_arr[day] = True

        draw *= self.ranges
        results.update({'ivols': np.squeeze(draw), 'ivols_accepted': np.mean(acceptance_arr)})

        return results

    def draw_innov_var(self, prior):
        """
        Returns a draw from the posterior of the volatility innovation variance.

        Parameters
        ----------
        prior : dict of floats
            The prior parameters

        Returns
        -------
        draw : dict of floats
        """
        regressor = mcmc.create_lag_matrix(np.log(self.ivols), self.lag_indices, prepend_ones=True)
        regressand = np.log(self.ivols[-regressor.shape[0]:])
        innov_var_draw = mcmc.draw_innovation_var(regressor, regressand, self.ivol_coeff, prior['scale'],
                                                  prior['shape'])

        draw = {'ivol_innov_var': np.squeeze(innov_var_draw)}

        return draw

    @property
    def ivols(self):
        return self._ivols

    @ivols.setter
    def ivols(self, val):
        self._ivols = np.ascontiguousarray(val, np.float64)

    @property
    def num_periods(self):
        return self.ivols.shape[0]


    def forecast_ivols(self, horizon):
        """
        Forecasts the volatilities forward.

        Parameters
        ----------
        horizon : positive int

        Returns
        -------
        future_ivols : 2d ndarray
        """

        future_ivols = simulate_ivols(self.ivol_coeff, self.lag_indices, self.innov_var, num_periods=horizon,
                                      initial_ivols=self.ivols[-1])

        return np.squeeze(future_ivols)


# noinspection PyIncorrectDocstring,PyTypeChecker
def simulate_gp_model(ivol_coeff, lag_indices=None, imean_innov_var=1, ivol_innov_var=1, num_periods=100,
                      num_obs_per_day=100, imeans=None, imeans_coeff=None, initial_ivol=None, initial_imean=None,
                      leverage=False, intra_day_log_vol_var=None):
    """ Simulates the gaussian process model

    Parameters
    ----------
    leverage
    initial_ivol
    initial_imean
    """
    ivol_coeff = np.asarray(ivol_coeff).astype(np.float64)
    imean_innov_var = (np.atleast_2d(imean_innov_var).astype(np.float64) if imean_innov_var is not None
                       else np.atleast_2d([1]).astype(np.float64))
    ivols = simulate_ivols(ivol_coeff, lag_indices, ivol_innov_var, num_periods, initial_ivols=initial_ivol)
    return_shape = num_periods, num_obs_per_day
    if intra_day_log_vol_var is not None:
        vols = np.exp(np.broadcast_to(np.log(ivols / num_obs_per_day).ravel(), (num_obs_per_day, num_periods)).T +
                np.sqrt(intra_day_log_vol_var) * np.random.standard_normal(return_shape) -
                (intra_day_log_vol_var/2))

    else:
        vols = np.broadcast_to(ivols.ravel() / num_obs_per_day, (num_obs_per_day, num_periods)).T
    if imeans is None:
        imeans_coeff = np.zeros(2 + leverage) if imeans_coeff is None else np.asarray(imeans_coeff)
        initial_imean = (np.ravel(initial_imean).astype(np.float64) if initial_imean is not None
                         else np.zeros(imeans_coeff.size-1 - leverage, dtype=np.float64))
        if leverage:
            imeans = forecast_imeans(num_periods, imeans_coeff, imean_innov_var, starting_point=initial_imean,
                                     ivol_forecast=ivols, ivol_coeff=ivol_coeff)
        else:
            imeans = forecast_imeans(num_periods, imeans_coeff, imean_innov_var, starting_point=initial_imean)
    else:
        imeans = np.ravel(imeans)

    means = np.broadcast_to(imeans[-num_periods:].ravel() / num_obs_per_day, (num_obs_per_day, num_periods)).T

    data =  means + np.random.standard_normal(return_shape) * np.sqrt(vols)
    # data = np.random.standard_normal(return_shape) * np.sqrt(vols)

    simulated_model = GPModel(data=data, ivol_coeff=ivol_coeff, lag_indices=lag_indices,
                              imean_innov_var=imean_innov_var, ivol_innov_var=ivol_innov_var,
                              imeans=imeans[-num_periods:], ivols=ivols, imean_coeff=imeans_coeff,
                              leverage=leverage)
    return simulated_model


def simulate_ivols(ivol_coeff=None, lag_indices=None, state_innov_var=1, num_periods=100, initial_ivols=None):
    """
    Simulates the volatilities

    Parameters
    ----------
    num_periods : positive int
    state_innov_var : 2d positive semi-definite ndarray
    lag_indices : ndarray of positive ints
    ivol_coeff : 2d square ndarray
    initial_ivols : 1d array-like
    """

    ivol_coeff, lag_indices = compute_trans_param_defaults(ivol_coeff, lag_indices)
    # from IPython.core.debugger import Tracer
    # Tracer()()
    state_trans = long_mem_state_trans(ivol_coeff, lag_indices)
    num_lags = state_trans.shape[0]
    mean = np.zeros(num_lags)
    mean[0] = ivol_coeff[0]
    if initial_ivols is not None:
        assert len(initial_ivols) >= num_lags, 'If you give an initial value, it must be long enough to contain the ' \
                                               'entire state.'
        log_ivols = np.log(initial_ivols[-num_lags:])
    else:
        log_ivols = np.full(num_lags, ivol_coeff[0] / (1 - np.sum(ivol_coeff[1:])), dtype=np.float64)

    innov_cov = np.zeros_like(state_trans)
    innov_cov[0, 0] = state_innov_var
    ivols = np.exp(mcmc.simulate_var(num_periods, innov_var=innov_cov, var_coeff=state_trans,
                                     mean=mean, initial_state=np.ascontiguousarray(np.flipud(log_ivols)))[:, 0])

    return np.squeeze(ivols)


# noinspection PyIncorrectDocstring
def compute_trans_param_defaults(ivol_coeff, lag_indices):
    """  Computes sensible default parameter values for ivol_coeff and lag_indices """

    if ivol_coeff is None and lag_indices is None:
        ivol_coeff = np.array([0, .9, 0, 0])
    elif ivol_coeff is None:
        ivol_coeff = np.zeros(lag_indices.size + 1)
        ivol_coeff[1] = .45
        ivol_coeff[2:] = .45 / (lag_indices.size - 1)

    if lag_indices is None:
        lag_indices = np.arange(1, ivol_coeff.size)

    return np.asarray(ivol_coeff), np.asarray(lag_indices)


@functools.lru_cache(maxsize=5)
def compute_uncond_cov(ivol_coeff, lag_indices, ivol_innov_var):
    """
    Computes the unconditional covariance of the model using the state space representation.

    If the model is not stationary, the unconditional covariance is not well defined, and a LinearAlgebraError
     is returned.

    Parameters
    ----------
    ivol_coeff : tuple
        The coefficients on the lags in the univariate form. It includes the intercept term.
    lag_indices : tuple of positive ints
        The indices of the lags. (It starts at 1 not 0).  That is in an AR(1) it is simply np.array([0])
    ivol_innov_var : positive float
        The innovation variance.

    Returns
    -------
        uncond_cov : 2d positive-definite ndarray
    """
    ivol_coeff = np.asarray(ivol_coeff)
    lag_indices = np.asarray(lag_indices)
    state_trans = long_mem_state_trans(ivol_coeff, lag_indices)
    flattened_var = np.array([ivol_innov_var if i == 0 else 0 for i in range(state_trans.size)])
    try:
        uncond_cov = scilin.solve(np.eye(state_trans.size) - scilin.kron(state_trans, state_trans),
                                  flattened_var).reshape(state_trans.shape)
    except np.linalg.linalg.LinAlgError:
        raise np.linalg.linalg.LinAlgError('The ivol_coeff values are not stationary.')

    return np.squeeze(uncond_cov)


def long_mem_state_trans(ivol_coeff, lag_indices):
    """
    Creates the state space transition matrix for the relevant coefficients and lag indices.

    Parameters
    ----------
    ivol_coeff : ndarray of floats
        The ivol_coeff from imean_coeff in matrix form. [intercept, coefficients].
    lag_indices : ndarray of positive ints
        The lags (1,7,30) for example in the long-memory representation.

    Returns
    -------
    state_trans : 2d ndarray of floats
    """
    num_states = np.max(lag_indices)
    first_row = np.zeros(num_states)
    first_row[lag_indices-1] = ivol_coeff[1:]
    state_trans = mcmc.factor_model_state_trans(first_row)

    return state_trans


def ivol_log_prior(ivol_coeff, ivols, lag_indices, uncond_cov, vol, day, ivol_innov_var):
    """

    Parameters
    ----------
    ivol_coeff : ndarray
    ivols : ndarray
    lag_indices : ndarray
    uncond_cov : ndarray
    vol : double
    day : nonnegative int
    ivol_innov_var : positive double

    Returns
    -------
    val : double
    """
    val = mcmc.cffi_wrap(realizeddensity_so.civol_log_prior,
                         ndarray_params=(np.ascontiguousarray(ivol_coeff, np.float64), ivols, np.intc(lag_indices),
                                         uncond_cov),
                         pod_params=(vol, np.float64(day), np.float64(ivol_innov_var), lag_indices.size, ivols.size))

    return val


def forecast_draws(results, gp_model, horizon=1):
    """

    Parameters
    ----------
    results : dict
        Dictionary containing the draws.
    gp_model : GPModel
        The model that was estimated. It is used to determine the values of parameters that are not estimated.
    horizon : positive int
        The number of days to forecast.

    Returns
    -------
    forecast_data : ndarray
    forecast_ivols : ndarray
    imeans_forecast : ndarray
    """

    nsims = results['ivol_coeff'].shape[0]
    forecast_data = np.empty((nsims, horizon, gp_model.num_obs_per_day))
    forecast_ivols = np.empty((nsims, horizon))
    imeans_forecast = np.empty((nsims, horizon))
    num_imean_lags = -results['imean_coeff'][0,0].size-2

    for idx in range(nsims):
        model = simulate_gp_model(ivol_coeff=results['ivol_coeff'][idx],
                                  lag_indices=gp_model.lag_indices,
                                  imean_innov_var=results['imean_innov_var'][idx],
                                  ivol_innov_var=results['ivol_innov_var'][idx],
                                  num_periods=horizon,
                                  num_obs_per_day=gp_model.num_obs_per_day,
                                  imeans_coeff=results['imean_coeff'][idx, :],
                                  initial_ivol=results['ivols'][idx],
                                  initial_imean=results['imeans'][idx, -num_imean_lags:],
                                  leverage=True
                                  )


        forecast_data[idx] = model.data
        forecast_ivols[idx] = model.ivols
        imeans_forecast[idx] = np.ravel(model.imeans)

    return forecast_data, forecast_ivols, imeans_forecast


def tridiag_inv(dim, diagval, off_diagval):
    """
    Computes the inverse of a tridiagonal symmetric toeplitz matrix.
    Parameters
    ----------
    dim : positive int
    diagval : float
    off_diagval : float

    Returns
    -------
    ndarray
    """
    inv_mat = mcmc.cffi_wrap(realizeddensity_so.ctridiag_inv,
                             ndarray_params=None,
                             pod_params=(dim, diagval, off_diagval),
                             return_shapes=((dim, dim),)
                             )

    return inv_mat


def tridiag_logdet(dim, diagval, off_diagval):
    """
    Computes the determinant of a tridiagonal toeplitz matrix.

    Parameters
    ----------
    dim : positive int
    diagval : double
    off_diagval : double

    Returns
    -------
    det:  double
    """

    return realizeddensity_so.tridiag_logdet(dim, diagval, off_diagval)


def tridiag_generator(dim, diag, left_diag, right_diag):
    """
    Generates a tridiagonal toeplitz matrix.
    Parameters
    ----------
    dim : positive int
    diag : double
    left_diag : double
    right_diag : double

    Returns
    -------
    ndarray
    """
    diag_arr = np.full((dim, dim), diag, dtype=np.float64)
    diag_plus_arr = np.full((dim, dim), left_diag, dtype=np.float64)
    diag_minus_arr = np.full((dim, dim), right_diag, dtype=np.float64)

    return_arr = (np.diag(np.diag(diag_arr)) + np.diag(np.diag(diag_plus_arr, 1), 1) +
                  np.diag(np.diag(diag_minus_arr, -1), -1))
    return return_arr


def tridiag_log_like(data, imean, ivol, noise_var):
    """
    Computes the log-likelihood of a model with an MA(1) noise term.

    It uses an optimized algorithm for this type of problem. It also assumes that the imean and ivol are given in their
    integrated form. In other words, to get the distribution of the individual elements you want to divide by the sample
    size.

    Parameters
    ----------
    data : array-like
    imean : float
    ivol : positive float
    noise_var : positive float

    Returns
    -------
    log_like : float
    """

    return mcmc.cffi_wrap(realizeddensity_so.cxiu_log_likelihood, (np.ascontiguousarray(data, np.float64),),
                          (data.size, imean, ivol, noise_var))


def forecast_imeans(horizon, imean_coeff, innov_var, starting_point=None, ivol_forecast=None, ivol_coeff=None):
    """
    Rolls the model forward to create a forecast of the model.

    Parameters
    ----------
    ivol_coeff
    starting_point
    innov_var
    imean_coeff
    ivol_forecast
    horizon : positive int
    ivol_coeff, 1d array-like, optional
        If ivol_forecast is given, ivol_coeff must be given as well.

    Returns
    -------
    future_imeans : 2d ndarray
    """


    future_imeans = list(starting_point)

    if ivol_forecast is None:
        log_vol_forecast = np.zeros(horizon)
        imean_coeff = np.hstack([np.ravel(imean_coeff),0])
    else:
        log_vol_forecast = np.log(ivol_forecast)

    num_lags = imean_coeff.size - 2
    ivol_coeff = np.ravel(ivol_coeff)
    log_vol_uncond_mean = ivol_coeff[0] / np.sum(1 - np.sum(ivol_coeff[1:]))

    for h in range(horizon):
        future_imeans.append(np.squeeze(imean_coeff[0] + imean_coeff[-1] * (log_vol_forecast[h] - log_vol_uncond_mean) +
                             np.flipud(imean_coeff[1:-1]).dot(future_imeans[-num_lags:]) + np.sqrt(innov_var) *
                             np.random.standard_normal()))

    imeans = np.squeeze(future_imeans[num_lags:])

    return imeans
