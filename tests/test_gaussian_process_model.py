from itertools import islice
import numpy as np
import pytest
from scipy import stats
from scipy.linalg import solve
import realizeddensity.gaussian_process_model as gpm


@pytest.fixture(scope='module')
def group_parameters():
    lag_indices = np.array([1, 2])
    vol_innov_var = 1
    beta = np.array([0, .5, 0])
    observed_vals = np.array([1, 2])

    return beta, lag_indices, observed_vals, vol_innov_var


# noinspection PyIncorrectDocstring, PyTypeChecker, PyShadowingNames
def test_compute_uncond_cov_ar1(group_parameters):
    """ Ensures that the function is computing the correct variance in the AR(1) case. """
    beta, lag_indices, observed_vals, vol_innov_var = group_parameters
    uncond_cov = gpm.compute_uncond_cov(tuple(beta), tuple(lag_indices), vol_innov_var)

    assert np.allclose(uncond_cov, np.array([[4, 2], [2, 4]]) / 3), 'The AR(1) unconditional variance is not correct.'


# noinspection PyIncorrectDocstring,PyShadowingNames
def test_compute_uncond_cov_nonstationary(group_parameters):
    """ The unconditional covariance is not defined in the nonstationary case. """
    _, lag_indices, observed_vals, vol_innov_var = group_parameters
    beta = np.array([0, .5, .5])

    with pytest.raises(np.linalg.linalg.LinAlgError):
        gpm.compute_uncond_cov(tuple(beta), tuple(lag_indices), vol_innov_var)


# noinspection PyShadowingNames
@pytest.fixture(scope='module')
def compute_uncond_cov(group_parameters):
    _, lag_indices, observed_vals, vol_innov_var = group_parameters
    beta = np.array([0, .5, .4])
    uncond_cov = gpm.compute_uncond_cov(tuple(beta), tuple(lag_indices), vol_innov_var)

    return beta, lag_indices, observed_vals, uncond_cov, vol_innov_var


# noinspection PyIncorrectDocstring,PyShadowingNames
def test_conditional_var_full_info(compute_uncond_cov):
    """ Ensures that we get the innovation variance if we know all of the lags.  """
    beta, lag_indices, observed_vals, uncond_cov, vol_innov_var = compute_uncond_cov

    _, cond_var = conditional_moments(lag_indices, observed_vals, uncond_cov, vol_innov_var, beta)

    assert cond_var == vol_innov_var, 'The variance is not the innovation variance even though I know the value of ' \
                                      'all of the lags.'


# noinspection PyIncorrectDocstring,PyShadowingNames
def test_conditional_var_one_lag(group_parameters):
    """ Ensures that we get the innovation variance back if we know lag(y) in y_t = 0 + \phi y_{t-2} + \eta_t  """
    beta, lag_indices, _, vol_innov_var = group_parameters
    observed_vals = np.array([.05])
    beta = np.array([0, 0, .9])

    uncond_cov = gpm.compute_uncond_cov(tuple(beta), tuple(lag_indices), vol_innov_var)
    _, cond_var = conditional_moments(lag_indices, observed_vals, uncond_cov, vol_innov_var, beta)

    # noinspection PyUnresolvedReferences
    ar2_cond_var = uncond_cov[0, 0]

    assert cond_var == ar2_cond_var, 'The variance is not the innovation variance even though I know the value of ' \
                                     'all of the lags.'


# noinspection PyShadowingNames
def test_ivol_log_prior(group_parameters):
    """ Ensures that the the two implementations of the prior are giving identical results.

    Parameters
    ----------
    group_parameters
    """

    beta, lag_indices, observed_vals, vol_innov_var = group_parameters
    uncond_cov = gpm.compute_uncond_cov(tuple(beta), tuple(lag_indices), vol_innov_var)
    num_periods = 100

    model = gpm.simulate_gp_model(beta, lag_indices, ivol_innov_var=vol_innov_var, num_periods=num_periods)

    day = num_periods // 2
    vol = model.ivols[day]

    cpp_val = model.ivol_log_prior(vol, day, beta)
    python_val = ivol_log_prior(beta, model.ivols, lag_indices, uncond_cov, vol, day, vol_innov_var)
    assert np.isclose(python_val, cpp_val), "The python and cpp implementations are not giving the same results, " \
                                            " when there are are meaningful lags."
    _, _, observed_vals, _ = group_parameters
    lag_indices = np.asarray([1, 7, 30])
    beta = np.asarray([-2, 0, 0, 0])
    uncond_cov = gpm.compute_uncond_cov(tuple(beta), tuple(lag_indices), vol_innov_var)
    model = gpm.simulate_gp_model(beta, lag_indices, ivol_innov_var=vol_innov_var, num_periods=num_periods)
    day = 1
    vol = model.ivols[1]
    python_val = ivol_log_prior(beta, model.ivols, lag_indices, uncond_cov, vol, day, vol_innov_var)
    cpp_val = model.ivol_log_prior(vol, day, beta)

    assert np.isclose(python_val, cpp_val), "The python and cpp implementations are not giving the same results when " \
                                            "there is only a mean."


def test_inv_symm_tridiag():
    dim, diagval, alpha = 40, .25, .75

    inv_mat = gpm.tridiag_inv(dim, diagval, alpha)
    inv_mat2 = np.linalg.inv(gpm.tridiag_generator(dim, diagval, alpha, alpha))

    assert np.allclose(inv_mat, inv_mat2), 'The two methods of computing the inverses are giving different results.'

    dim, diagval, alpha = 40, 2, np.sqrt(2)
    inv_mat = gpm.tridiag_inv(dim, diagval, alpha)
    inv_mat2 = np.linalg.inv(gpm.tridiag_generator(dim, diagval, alpha, alpha))

    assert np.allclose(inv_mat, inv_mat2), 'The two methods of computing the inverses are giving different results' \
                                           'if the discriminant is zero.'

    dim, diagval, alpha = 40, 2, -np.sqrt(2)
    inv_mat = gpm.tridiag_inv(dim, diagval, alpha)
    inv_mat2 = np.linalg.inv(gpm.tridiag_generator(dim, diagval, alpha, alpha))

    assert np.allclose(inv_mat, inv_mat2), 'The two methods of computing the inverses are giving different results' \
                                           'if the discriminant is zero, and the off-diagonal value is n.'

    dim, diagval, alpha = 999, .2, -np.sqrt(2)
    inv_mat = gpm.tridiag_inv(dim, diagval, alpha)
    inv_mat2 = np.linalg.inv(gpm.tridiag_generator(dim, diagval, alpha, alpha))

    assert np.allclose(inv_mat, inv_mat2), 'It is not giving the correct results for large covariance matrices.' \
                                           'where we are still using the one algorithm.'

    dim, diagval, alpha = 450, .2, 0
    inv_mat = gpm.tridiag_inv(dim, diagval, alpha)
    inv_mat2 = np.linalg.inv(gpm.tridiag_generator(dim, diagval, alpha, alpha))

    assert np.allclose(inv_mat, inv_mat2), 'Make sure it works when the noise variance is zero.'


def test_tridiag_toeplitz_det():
    """Ensures that the method for computing the determinant of a tridiagonal toeplitz matrix doing the right thing."""
    dim, diagval, left_diag = 2000, .3, .1,  # Test it on general input.

    tridiag_mat = gpm.tridiag_generator(dim, diagval, left_diag, left_diag)
    assert np.isclose(np.linalg.slogdet(tridiag_mat)[1], gpm.tridiag_logdet(dim, diagval, left_diag)), \
        'The two methods for computing the determinants are not the same.'

    # Test the case where the roots are the same.
    dim, diagval, left_diag = 3, 1.0, .5,
    tridiag_mat = gpm.tridiag_generator(dim, diagval, left_diag, left_diag)
    assert np.isclose(np.linalg.slogdet(tridiag_mat)[1], gpm.tridiag_logdet(dim, diagval, left_diag)), \
        'The case with two identical roots does not work.'

    dim, diagval, left_diag = 50, 3, -.1
    tridiag_mat = gpm.tridiag_generator(dim, diagval, left_diag, left_diag)
    assert np.isclose(np.linalg.slogdet(tridiag_mat)[1], gpm.tridiag_logdet(dim, diagval, left_diag)), \
        'Ensure that the values are right with negative off diagonal elements. '

    dim, diagval, left_diag = 450, 3, 0
    tridiag_mat = gpm.tridiag_generator(dim, diagval, left_diag, left_diag)
    assert np.isclose(np.linalg.slogdet(tridiag_mat)[1], gpm.tridiag_logdet(dim, diagval, left_diag)), \
        'Make sure that we have the right determinant when the noise variance is zero. '


def test_tridiag_log_like():
    """Ensures that the method of computing the tridiagonal log-likelihood is giving correct results."""
    dim, imean, ivol, noise_var = 20, .1, .01, .002
    data = imean / dim + np.sqrt(ivol / dim + noise_var) * np.random.standard_normal(dim)
    tridiag_mat = gpm.tridiag_generator(dim, ivol / data.size + 2 * noise_var, - noise_var, -noise_var)

    scipy_log_like = stats.multivariate_normal.logpdf(data, mean=np.full(data.size, imean / dim), cov=tridiag_mat)
    fast_log_like = gpm.tridiag_log_like(data, imean, ivol, noise_var)
    assert np.isclose(scipy_log_like, fast_log_like), 'The two implementations of the log-likelihood are giving ' \
                                                      'different values.'

    dim, imean, ivol, noise_var = 998, .1, .01, .002
    data = imean / dim + np.sqrt(ivol / dim + noise_var) * np.random.standard_normal(dim)
    tridiag_mat = gpm.tridiag_generator(dim, ivol / data.size + 2 * noise_var, - noise_var, -noise_var)

    scipy_log_like = stats.multivariate_normal.logpdf(data, mean=np.full(data.size, imean / dim), cov=tridiag_mat)
    fast_log_like = gpm.tridiag_log_like(data, imean, ivol, noise_var)
    assert np.isclose(scipy_log_like, fast_log_like), 'Making sure that it works for large dimensions as well.'

    dim, imean, ivol, noise_var = 450, 0, .01, 0
    data = imean / dim + np.sqrt(ivol / dim + noise_var) * np.random.standard_normal(dim)
    tridiag_mat = gpm.tridiag_generator(dim, ivol / data.size + 2 * noise_var, - noise_var, -noise_var)
    scipy_log_like = stats.multivariate_normal.logpdf(data, mean=np.full(data.size, imean / dim), cov=tridiag_mat)
    fast_log_like = gpm.tridiag_log_like(data, imean, ivol, noise_var)
    assert np.isclose(scipy_log_like, fast_log_like), 'Making sure it works when the noise variance is zero.'


def test_long_mem_state_trans():
    ivol_coeff = np.random.standard_normal(10)
    lag_indices = np.array([1, 2, 6, 7])

    assert (np.allclose(long_mem_state_trans(ivol_coeff, lag_indices),
                       gpm.long_mem_state_trans(ivol_coeff, lag_indices)),
            'The ne method for computing the state transition matrix must return the same value as the old one. ')


def conditional_moments(lag_indices, observed_vals, uncond_cov, ivol_innov_var, beta):
    """
    Computes the conditional moments of the observation.
    Parameter
    ----------
    lag_indices : iterable of positive ints
        The lags of the indices.  It starts at 1 not 0.
    observed_vals : ndarray of floats
        The values we observe.  It must have length less than lag_indices.
    uncond_cov : 2d positive definite ndarray of floats
        The unconditional covariance matrix.
    ivol_innov_var : positive float
        The variance of the innovation to the log-volatility equation.

    Parameters
    ----------
    beta
    ivol_innov_var
    uncond_cov
    observed_vals
    lag_indices

    Returns
    -------
    double

    """

    if observed_vals.size == lag_indices.size:
        cond_var = ivol_innov_var
        cond_mean = np.dot(beta, np.insert(np.log(observed_vals), 0, 1))
    elif observed_vals.size == 0:
        cond_var = uncond_cov[0, 0]
        cond_mean = beta[0] / (1 - np.sum(beta[1:]))
    else:
        uncond_mean = beta[0] / (1 - np.sum(beta[1:]))
        indices = np.fromiter(islice(lag_indices, observed_vals.size),
                              dtype=np.int, count=observed_vals.size)
        cond_var = (uncond_cov[0, 0] - uncond_cov[0, indices] @
                    solve(uncond_cov[indices, :][:, indices], uncond_cov[indices, 0]))
        cond_mean = uncond_mean + uncond_cov[0, indices] @ solve(uncond_cov[indices, :][:, indices],
                                                                 np.log(observed_vals) - uncond_mean)
    return cond_mean, cond_var


def ivol_log_prior(ivol_coeff, ivols, lag_indices, uncond_cov, vol, day, ivol_innov_var):
    """

    Parameters
    ----------
    ivol_coeff
    ivols
    lag_indices
    uncond_cov
    vol
    day
    ivol_innov_var

    Returns
    -------

    """
    pretend_vol = ivols
    pretend_vol[day] = vol
    lg_prior = 0
    arg_arr = np.append(day, day + lag_indices)
    for idx in arg_arr[(arg_arr < ivols.shape[0])]:
        param_arr = idx - lag_indices
        lg_prior += log_prior_density(pretend_vol[idx], pretend_vol[param_arr[param_arr > 0]], ivol_coeff, lag_indices,
                                      uncond_cov, ivol_innov_var)

    return lg_prior


def log_prior_density(vol, observed_vals, ivol_coeff, lag_indices, uncond_cov, ivol_innov_var):
    """
     Computes the log prior density of a gaussian random variable with dynamics of the form y_t =
     \beta_0 +  \sum_i \beta_i y_{t-i} + e_t.
    Parameters
    ----------
    vol : double
        The value to evaluate the model at i.e. (y_t)
    observed_vals : 1d ndarray of doubles
        The lagged values.
    ivol_coeff : ndarray
        The betas.
    lag_indices : the indices of the lags. They must be at least 1.
    uncond_cov : 2d ndarray
        The covariance matrix of the model in state-space form.  It will be of size max(lag_indices) x max(lag_indices)
    ivol_innov_var : positive double

    Returns
    -------
    double
    The log prior value of the vol.

    """
    cond_mean, cond_var = conditional_moments(lag_indices, observed_vals, uncond_cov, ivol_innov_var, ivol_coeff)

    return stats.norm.logpdf(x=np.log(vol), loc=cond_mean, scale=np.sqrt(cond_var))

def long_mem_state_trans(ivol_coeff, lag_indices):
    """
    Creates the state space transition matrix for the relevant coefficients and lag indices.

    Parameters
    ----------
    ivol_coeff : ndarray of floats
        The ivol_coeff from imean_coeff in matrix form. [intercept, coefficients].
    lag_indices : ndarray of positive ints
        The lags (1,7,30) for example in the long-memory representation.

    Returns
    -------
    state_trans : 2d ndarray of floats
    """
    num_states = np.max(lag_indices)
    state_trans = np.zeros((num_states, num_states))
    for idx, val in enumerate(lag_indices):
        state_trans[0, val - 1] = ivol_coeff[idx + 1]
    for idx in range(1, state_trans.shape[0]):
        state_trans[idx, idx - 1] = 1
    return state_trans