# We import the necessary files. The sys.path.insert line allows us to import files from the directory above.
import numpy as np
import pytest
import pandas as pd
# import finite_scale_mixture_funcs as fsmf

__author__ = 'sangrey'


# We start by setting up some data to be used in the testing.
# This function simulates data from the model given a vector of the sigmas and a vector of proportions,.
# Index-thing is the pandas index that you are simulating data to align the result with.
# noinspection PyUnresolvedReferences
@pytest.fixture
def fake_data():
    sigma_vec = np.random.uniform(.01, 5, size=5)
    prop = np.array([.2, .4, .3, .07, .03])
    fake_index = pd.date_range('1/1/2013', periods=500, freq='1min')
    return fsmf.simulate_data(sigma_vec, prop, fake_index)[0].values






