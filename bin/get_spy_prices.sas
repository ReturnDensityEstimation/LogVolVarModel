libname taq'/wrds/taq/sasdata'; *location of the data;
libname local '/home/upenn/sangrey'; *My home directory;

%let start_time = '9:00:00't; *starting time;
%let interval_seconds = 10; *interval is 15 Seconds; 

*proc import datafile="~/s_and_p_500_tickers.csv"
     out=constituents
     dbms=csv
     replace;
*run;




*proc sql noprint;
	*select * into :const_macro
	separated by ' '
	from constituents;
*quit;




data allyear;
*	array ticker_arr[2] $   ('GE' 'C'); 
	*You have to change the number here if the number of variables change; 
	set taq.ct_2013:; 
	where symbol in ('SPY') and time between '9:00:00't and '16:30:00't
	and price>0 and size>0 and CORR in (0,1,2) and COND not in ("O" "Z" "B" "T" "L" "G" "W" "J" "K");
run;

proc sort; by symbol date time; run;



data xtemp2; 
	set allyear;
	by symbol date time;
	format itime rtime time12.;	if first.symbol=1 or first.date=1 then do;
		*Initialize time and price when new symbol or date starts;
		rtime = time;
		iprice=price;
		itime = &start_time;
	end;
	if time >= itime then do;
		output; *rtime and iprice hold the last observation values;
		itime = time + &interval_seconds;
		do while(time >= itime); *need to fill in all time intervals;
			output;
			itime = itime + &interval_seconds;
		end;
	end;
	
	rtime = time;
	iprice = price;
	retain itime rtime iprice; *carry time and price values forward;
	*keep symbol date itime iprice rtime;

run; 



Title "Final output -- 15 Sec interval";
proc print data = xtemp2;
	var symbol date rtime itime iprice; 
run;

*Saving the dataset in SAS format;
*Note: change the name if you change ther interval;
proc export 
	data=WORK.XTemp2
 	outfile='~/spy_raw.csv'
 	dbms=csv
 	replace;
run; 


