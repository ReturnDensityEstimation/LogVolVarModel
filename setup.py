from setuptools import setup, Command
import os


# noinspection PyMethodMayBeStatic
class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system('rm -vrf ./build ./dist ./*.egg-info')


setup(
    name='realizeddensity',
    version='0.0.1',
    packages=['tests', 'realizeddensity'],
    url='git@gitlab.com:sangrey/RealizedDensityEstimation.git',
    license='Apache',
    author='Paul Sangrey',
    author_email='sangrey@sas.upenn.edu',
    description='',
    test_suite='pytest',
    requires=['numpy', 'scipy', 'bayesiankalman', 'sklearn', 'numba', 'pytables',
              'pytest', 'hypothesis', 'statsmodels', 'pandas', 'tqdm', 'cffi'],
    cmdclass={'clean': CleanCommand, }
)
